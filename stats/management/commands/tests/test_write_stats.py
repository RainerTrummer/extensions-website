from io import StringIO

from django.core.management import call_command
from django.test import TestCase

from common.tests.factories.extensions import create_approved_version
from stats.models import ExtensionView, ExtensionDownload, ExtensionCountedStat


class WriteStatsCommandTest(TestCase):
    fixtures = ['dev', 'licenses']

    def test_command_updates_extensions_view_counters(self):
        out = StringIO()
        extension = create_approved_version().extension
        ExtensionView.objects.bulk_create(
            [
                ExtensionView(extension_id=extension.pk, ip_address='192.19.10.10'),
                ExtensionView(extension_id=extension.pk, ip_address='192.19.10.11'),
            ]
        )
        old_date_modified = extension.date_modified

        call_command('write_stats', stdout=out)

        extension.refresh_from_db()
        self.assertEqual(extension.view_count, 2)
        self.assertEqual(extension.download_count, 0)
        # auto-update field should not have changed on recount
        self.assertEqual(old_date_modified, extension.date_modified)
        # tables storing individual views still have them
        self.assertEqual(ExtensionView.objects.count(), 2)
        self.assertEqual(ExtensionDownload.objects.count(), 0)
        # last seen ID for this counter should have been stored
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='view_count').last_seen_id,
            ExtensionView.objects.order_by('-id').first().pk,
        )
        self.assertEqual(ExtensionCountedStat.objects.filter(field='download_count').count(), 0)

    def test_command_updates_extensions_download_counters(self):
        out = StringIO()
        extension = create_approved_version().extension
        ExtensionDownload.objects.bulk_create(
            [
                ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.10'),
                ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.11'),
            ]
        )
        old_date_modified = extension.date_modified

        call_command('write_stats', stdout=out)

        extension.refresh_from_db()
        self.assertEqual(extension.view_count, 0)
        self.assertEqual(extension.download_count, 2)
        # auto-update field should not have changed on recount
        self.assertEqual(old_date_modified, extension.date_modified)
        # tables storing individual views still have them
        self.assertEqual(ExtensionView.objects.count(), 0)
        self.assertEqual(ExtensionDownload.objects.count(), 2)
        # last seen ID for this counter should have been stored
        self.assertEqual(ExtensionCountedStat.objects.filter(field='view_count').count(), 0)
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='download_count').last_seen_id,
            ExtensionDownload.objects.order_by('-id').first().pk,
        )

    def test_command_adds_extensions_view_counters(self):
        out = StringIO()
        extension = create_approved_version().extension
        extension.view_count = 10
        extension.save(update_fields={'view_count'})
        ExtensionView.objects.bulk_create(
            [
                ExtensionView(extension_id=extension.pk, ip_address='192.19.10.10'),
                ExtensionView(extension_id=extension.pk, ip_address='192.19.10.11'),
            ]
        )
        old_date_modified = extension.date_modified

        call_command('write_stats', stdout=out)

        extension.refresh_from_db()
        self.assertEqual(extension.view_count, 12)
        self.assertEqual(extension.download_count, 0)
        # auto-update field should not have changed on recount
        self.assertEqual(old_date_modified, extension.date_modified)
        # tables storing individual views still have them
        self.assertEqual(ExtensionView.objects.count(), 2)
        self.assertEqual(ExtensionDownload.objects.count(), 0)
        # last seen ID for this counter should have been stored
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='view_count').last_seen_id,
            ExtensionView.objects.order_by('-id').first().pk,
        )
        self.assertEqual(ExtensionCountedStat.objects.filter(field='download_count').count(), 0)

    def test_command_adds_extensions_download_counters(self):
        out = StringIO()
        extension = create_approved_version().extension
        extension.download_count = 10
        extension.save(update_fields={'download_count'})
        ExtensionDownload.objects.bulk_create(
            [
                ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.10'),
                ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.11'),
            ]
        )
        old_date_modified = extension.date_modified

        call_command('write_stats', stdout=out)

        extension.refresh_from_db()
        self.assertEqual(extension.view_count, 0)
        self.assertEqual(extension.download_count, 12)
        # auto-update field should not have changed on recount
        self.assertEqual(old_date_modified, extension.date_modified)
        # tables storing individual views still have them
        self.assertEqual(ExtensionView.objects.count(), 0)
        self.assertEqual(ExtensionDownload.objects.count(), 2)
        # last seen ID for this counter should have been stored
        self.assertEqual(ExtensionCountedStat.objects.filter(field='view_count').count(), 0)
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='download_count').last_seen_id,
            ExtensionDownload.objects.order_by('-id').first().pk,
        )

    def test_command_updates_extensions_both_download_and_view_counters(self):
        out = StringIO()
        extension = create_approved_version().extension
        extension.download_count = 5
        extension.view_count = 4
        extension.save(update_fields={'download_count', 'view_count'})
        ExtensionView.objects.bulk_create(
            [
                ExtensionView(extension_id=extension.pk, ip_address='192.19.10.10'),
                ExtensionView(extension_id=extension.pk, ip_address='192.19.10.11'),
            ]
        )
        ExtensionDownload.objects.bulk_create(
            [
                ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.10'),
                ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.11'),
            ]
        )
        old_date_modified = extension.date_modified

        call_command('write_stats', stdout=out)

        extension.refresh_from_db()
        self.assertEqual(extension.view_count, 6)
        self.assertEqual(extension.download_count, 7)
        # auto-update field should not have changed on recount
        self.assertEqual(old_date_modified, extension.date_modified)
        # tables storing individual views still have them
        self.assertEqual(ExtensionView.objects.count(), 2)
        self.assertEqual(ExtensionDownload.objects.count(), 2)
        # last seen ID for both counters should have been stored
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='view_count').last_seen_id,
            ExtensionView.objects.order_by('-id').first().pk,
        )
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='download_count').last_seen_id,
            ExtensionDownload.objects.order_by('-id').first().pk,
        )

    def test_command_updates_extensions_both_download_and_view_counters_uses_last_seen_id(self):
        out = StringIO()
        initial_view_count, initial_download_count = 4, 5
        extension = create_approved_version().extension
        extension.download_count = initial_download_count
        extension.view_count = initial_view_count
        extension.save(update_fields={'download_count', 'view_count'})
        download_count_starts_here = ExtensionDownload(
            extension_id=extension.pk, ip_address='192.19.10.15'
        )
        view_count_starts_here = ExtensionView(extension_id=extension.pk, ip_address='192.19.10.13')
        view_next = ExtensionView(extension_id=extension.pk, ip_address='127.0.0.1')
        download_next = ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.11')
        ExtensionView.objects.bulk_create(
            [
                ExtensionView(extension_id=extension.pk, ip_address='192.19.10.10'),
                ExtensionView(extension_id=extension.pk, ip_address='192.19.10.11'),
                view_count_starts_here,
                ExtensionView(extension_id=extension.pk, user_id=15),
                ExtensionView(extension_id=extension.pk, user_id=16),
                view_next,
            ]
        )
        ExtensionDownload.objects.bulk_create(
            [
                ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.10'),
                ExtensionDownload(extension_id=extension.pk, user_id=66),
                download_count_starts_here,
                ExtensionDownload(extension_id=extension.pk, user_id=1324),
                download_next,
            ]
        )
        ExtensionCountedStat.objects.bulk_create(
            [
                ExtensionCountedStat(field='view_count', last_seen_id=view_count_starts_here.pk),
                ExtensionCountedStat(
                    field='download_count', last_seen_id=download_count_starts_here.pk
                ),
            ]
        )
        old_date_modified = extension.date_modified

        call_command('write_stats', stdout=out)

        extension.refresh_from_db()
        self.assertEqual(extension.view_count, initial_view_count + 3)
        self.assertEqual(extension.download_count, initial_download_count + 2)
        # auto-update field should not have changed on recount
        self.assertEqual(old_date_modified, extension.date_modified)
        # tables storing individual views still have them
        self.assertEqual(ExtensionView.objects.count(), 6)
        self.assertEqual(ExtensionDownload.objects.count(), 5)
        # last seen ID for both counters should have been stored
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='view_count').last_seen_id,
            view_next.pk,
        )
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='download_count').last_seen_id,
            download_next.pk,
        )

        # do another count
        call_command('write_stats', stdout=out)

        # no changes, because no new visits recorded
        extension.refresh_from_db()
        self.assertEqual(extension.view_count, initial_view_count + 3)
        self.assertEqual(extension.download_count, initial_download_count + 2)
        self.assertEqual(ExtensionView.objects.count(), 6)
        self.assertEqual(ExtensionDownload.objects.count(), 5)
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='view_count').last_seen_id,
            view_next.pk,
        )
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='download_count').last_seen_id,
            download_next.pk,
        )
