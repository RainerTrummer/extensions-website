from django.urls import path

import apitokens.views


app_name = 'apitokens'
urlpatterns = [
    path('settings/tokens/', apitokens.views.TokensView.as_view(), name='list'),
    path('tokens/create/', apitokens.views.CreateTokenView.as_view(), name='create'),
    path('tokens/delete/<int:pk>/', apitokens.views.DeleteTokenView.as_view(), name='delete'),
]
