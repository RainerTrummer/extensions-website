import logging

from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.views.generic.edit import CreateView
from django.views.generic import ListView, DeleteView
from django.shortcuts import reverse
from django.utils.safestring import mark_safe
from django.urls import reverse_lazy


from .models import UserToken

logger = logging.getLogger(__name__)


class TokensView(LoginRequiredMixin, ListView):
    model = UserToken
    context_object_name = 'tokens'

    def get_queryset(self):
        return self.request.user.tokens.all()


class UserTokenForm(forms.ModelForm):
    class Meta:
        model = UserToken
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)  # Get user information from kwargs
        super().__init__(*args, **kwargs)

    def clean_name(self):
        name = self.cleaned_data.get('name')
        if UserToken.objects.filter(user=self.user, name=name).exists():
            error_message = mark_safe(f'A token with this name already exists: <b>{name}</b>.')
            raise forms.ValidationError(error_message)
        return name


class CreateTokenView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = UserToken
    form_class = UserTokenForm
    template_name = 'apitokens/apitoken_create.html'
    success_message = (
        "Your new token has been generated. Copy it now as it will not be shown again."
    )

    def get_success_url(self):
        return reverse('apitokens:list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.instance.user = self.request.user

        token_key = UserToken.generate_token_key()
        form.instance.token_hash = UserToken.generate_hash(token_key)
        form.instance.token_prefix = UserToken.generate_token_prefix(token_key)

        messages.info(self.request, token_key)
        return super().form_valid(form)


class DeleteTokenView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = UserToken
    template_name = 'apitokens/apitoken_confirm_delete.html'
    success_url = reverse_lazy('apitokens:list')
    success_message = "Token deleted successfully"

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)
