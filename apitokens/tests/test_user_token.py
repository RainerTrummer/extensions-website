from datetime import datetime
from django.utils import timezone

from django.test import TestCase
from django.urls import reverse

from apitokens.models import UserToken
from common.tests.factories.users import UserFactory
from common.tests.utils import create_user_token


class UserTokenTest(TestCase):
    def setUp(self) -> None:
        self.user = UserFactory()
        self.client.force_login(self.user)
        self.assertEqual(UserToken.objects.count(), 0)
        return super().setUp()

    def test_token_displayed_only_once(self):
        response = self.client.post(
            reverse('apitokens:create'),
            {
                'name': 'Test Token',
            },
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('apitokens:list'))
        self.assertEqual(UserToken.objects.count(), 1)
        token = UserToken.objects.first()

        # Check if the success message with the token value is displayed
        messages = list(response.wsgi_request._messages)
        self.assertEqual(len(messages), 2)

        token_key = messages[0].message
        self.assertIn(token.token_prefix, token_key)
        self.assertIn('Your new token has been generated', messages[1].message)

        token_hash = UserToken.generate_hash(token_key)
        self.assertEqual(token, UserToken.objects.get(token_hash=token_hash))

        # Verify the token value is shown only on the creation page
        response = self.client.get(reverse('apitokens:list'))
        self.assertNotContains(response, token_key)

    def test_list_page_does_not_display_full_token_value(self):
        token, token_key = create_user_token(user=self.user, name='Test Token')

        response = self.client.get(reverse('apitokens:list'))
        self.assertContains(response, str(token.token_prefix))
        self.assertNotContains(response, str(token_key))

    def test_list_page_shows_last_access_time(self):
        token = UserToken.objects.create(user=self.user, name='Test Token')

        # Create a timezone-aware datetime object.
        date_last_access_str = '1994-01-02 10:10:36'
        date_last_access_naive = datetime.strptime(date_last_access_str, '%Y-%m-%d %H:%M:%S')
        date_last_access_aware = timezone.make_aware(
            date_last_access_naive, timezone.get_default_timezone()
        )
        token.date_last_access = date_last_access_aware

        # Format the datetime to match the expected response format.
        formatted_date = (
            date_last_access_aware.strftime('%b. %-d, %Y, %-I:%M %p')
            .replace('AM', 'a.m.')
            .replace('PM', 'p.m.')
        )

        token.save()
        response = self.client.get(reverse('apitokens:list'))
        self.assertContains(response, formatted_date)
