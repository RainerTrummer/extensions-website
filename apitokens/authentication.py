from django.utils import timezone

from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed
from .models import UserToken
from utils import clean_ip_address


class UserTokenAuthentication(BaseAuthentication):
    def authenticate(self, request):
        auth_header = request.headers.get('Authorization')

        if not auth_header:
            return None

        try:
            token_type, token_key = auth_header.split()
            if token_type.lower() != 'bearer':
                return None
        except ValueError:
            return None

        try:
            token_hash = UserToken.generate_hash(token_key=token_key)
            token = UserToken.objects.get(token_hash=token_hash)
        except UserToken.DoesNotExist:
            raise AuthenticationFailed('Invalid token')

        token.ip_address_last_access = clean_ip_address(request)
        token.date_last_access = timezone.now()
        token.save(update_fields={'ip_address_last_access', 'date_last_access'})

        return (token.user, token)
