from django.urls import path, include

import notifications.views as views

app_name = 'notifications'
urlpatterns = [
    path(
        'notifications/',
        include(
            [
                path('', views.NotificationsView.as_view(), name='notifications'),
                path(
                    'mark-read-all/',
                    views.MarkReadAllView.as_view(),
                    name='notifications-mark-read-all',
                ),
                path(
                    '<int:pk>/mark-read/',
                    views.MarkReadView.as_view(),
                    name='notifications-mark-read',
                ),
            ],
        ),
    ),
]
