"""Lists permissions allowed for add-ons."""

from django.utils.translation import gettext_lazy as _


class VERSION_PERMISSION_FILE:
    id = 1
    name = _('File')
    slug = 'file'
    help = 'Add-on expects to have access to filesytem.'


class VERSION_PERMISSION_NETWORK:
    id = 2
    name = _('Network')
    slug = 'network'
    help = 'Add-on expects to have internet access.'


ALL_VERSION_PERMISSIONS = (
    VERSION_PERMISSION_FILE,
    VERSION_PERMISSION_NETWORK,
)
