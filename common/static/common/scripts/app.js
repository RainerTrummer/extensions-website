(function() {
  // Create function agreeWithTerms
  function agreeWithTerms() {
    const agreeWithTermsTrigger = document.querySelectorAll('.js-agree-with-terms-trigger');
    const agreeWithTermsCheckbox = document.querySelector('.js-agree-with-terms-checkbox');
    const agreeWithTermsFileInput = document.querySelector('.js-submit-form-file-input');
    let agreeWithTermsBtnSubmit = document.querySelector('.js-agree-with-terms-btn-submit');

    if (!agreeWithTermsCheckbox) {
      // Stop function execution if agreeWithTermsCheckbox is not present
      return;
    }

    // Both the file input and checkbox can trigger the submit button.
    agreeWithTermsTrigger.forEach((el) => {
      el.addEventListener('change', function() {

        // Check if checkbox is checked, and file input has a file selected.
        let is_allowed = (agreeWithTermsCheckbox.checked == true) && (agreeWithTermsFileInput.value != "");

        if (is_allowed) {
          agreeWithTermsBtnSubmit.removeAttribute('disabled');
        } else {
          agreeWithTermsBtnSubmit.setAttribute('disabled', true);
        }
      });
    });
  }


  // Create function submitFormFileInputClear
  // When the user chooses a new file to upload, clear the error classes.
  function submitFormFileInputClear() {
    const submitFormFileInput = document.querySelector('.js-submit-form-file-input');

    if (!submitFormFileInput) {
      // Stop function execution if submitFormFileInput is not present
      return;
    }

    submitFormFileInput.addEventListener('change', function(e) {
      e.target.classList.remove('is-invalid');
    });
  }


  // Create function btnBack
  function btnBack() {
    const btnBack = document.querySelectorAll('.js-btn-back');

    btnBack.forEach(function(item) {
      item.addEventListener('click', function(e) {
        e.preventDefault();
        window.history.back();
      });
    });
  }

  // Create function commentForm
  function commentForm() {
    const commentForm = document.querySelector('.js-comment-form');
    if (!commentForm) {
      return;
    }

    const commentFormSelect = commentForm.querySelector('select');
    if (!commentFormSelect) {
      return;
    }

    // Create event comment form select change
    commentFormSelect.addEventListener('change', function(e) {
      let value = e.target.value;
      let verb = 'Comment';
      const activitySubmitButton = document.getElementById('activity-submit');
      activitySubmitButton.classList.remove('btn-primary', 'btn-success', 'btn-warning');

      // Hide or show comment form msg on change
      if (value == 'AWC') {
        verb = 'Set as Awaiting Changes';
        activitySubmitButton.classList.add('btn-warning');
      } else if (value == 'AWR') {
        verb = 'Set as Awaiting Review';
      } else if (value == 'APR') {
        verb = 'Approve!';
        activitySubmitButton.classList.add('btn-success');
      } else {
        activitySubmitButton.classList.add('btn-primary');
      }

      activitySubmitButton.querySelector('span').textContent = verb;
    });
  }

  // Create function copyInstallUrl
  function copyInstallUrl() {
    function init() {
      // Create variables
      const btnInstall = document.querySelector('.js-btn-install');
      const btnInstallAction = document.querySelector('.js-btn-install-action');
      const btnInstallGroup = document.querySelector('.js-btn-install-group');
      const btnInstallDrag = document.querySelector('.js-btn-install-drag');
      const btnInstallDragGroup = document.querySelector('.js-btn-install-drag-group');

      if (btnInstall == null) {
        return;
      }

      // Get data install URL
      const btnInstallUrl = btnInstall.getAttribute('data-install-url');

      btnInstall.addEventListener('click', function() {
        // Hide btnInstallGroup
        btnInstallGroup.classList.add('d-none');

        // Show btnInstallAction
        btnInstallAction.classList.add('show');
      });

      // Drag btnInstallUrl
      btnInstallDrag.addEventListener('dragstart', function(e) {
        // Set data install URL to be transferred during drag
        e.dataTransfer.setData('text/plain', btnInstallUrl);

        // Set drag area active
        btnInstallDragGroup.classList.add('opacity-50');
      });

      // Undrag btnInstallUrl
      btnInstallDrag.addEventListener('dragend', function() {
        // Set drag area inactive
        btnInstallDragGroup.classList.remove('opacity-50');
      });
    }

    init();
  }

  // Create function navGlobalLinkSearch
  function navGlobalLinkSearch() {
    const navGlobalLinkSearch = document.querySelector('.js-nav-global-link-search');
    const navGlobalLinkSearchToggle = document.querySelector('.js-nav-global-link-search-toggle');

    // Toggle navbar search on small screens
    navGlobalLinkSearchToggle.addEventListener('click', function() {
      this.classList.toggle('is-active');

      if (this.classList.contains('is-active')) {
        // Show navGlobalLinkSearch
        navGlobalLinkSearch.classList.add('is-active');
      } else {
        navGlobalLinkSearch.classList.remove('is-active');
      }
    });
  }

  // Create function init
  function init() {
    agreeWithTerms();
    submitFormFileInputClear();
    btnBack();
    commentForm();
    copyInstallUrl();
    navGlobalLinkSearch();
  }

  document.addEventListener('DOMContentLoaded', function() {
    init();
  });
}())
