import logging
import random

from django.core.management.base import BaseCommand
from django.db import transaction

from common.tests.factories.extensions import create_approved_version, create_version
from common.tests.factories.files import FileFactory, ImageFactory
from common.tests.factories.teams import TeamFactory
from files.models import File
from constants.version_permissions import VERSION_PERMISSION_FILE, VERSION_PERMISSION_NETWORK
from constants.licenses import LICENSE_GPL2, LICENSE_GPL3
from extensions.models import Extension, Tag

FILE_SOURCES = {
    "blender-kitsu": {
        "file": 'files/ed/ed656b177b01999e6fcd0e37c34ced471ef88c89db578f337e40958553dca5d2.zip',
        "hash": "sha256:3d2972a6f6482e3c502273434ca53eec0c5ab3dae628b55c101c95a4bc4e15b2",
        "size": 856650,
    },
}
PREVIEW_SOURCES = (
    'images/b0/b03fa981527593fbe15b28cf37c020220c3d83021999eab036b87f3bca9c9168.png',
    'images/be/bee9e018e80aee1176019a7ef979a3305381b362d5dedc99b9329a55bfa921b9.png',
    'images/cf/cfe6569d16ed50d3beffc13ccb3c4375fe3b0fd734197daae3c80bc6d496430f.png',
    'images/f4/f4c377d3518b6e17865244168c44b29f61c702b01a34aeb3559f492b1b744e50.png',
    'images/dc/dc8d57c6af69305b2005c4c7c71eff3db855593942bebf3c311b4d3515e955f0.png',
    'images/09/091b7bc282e8137a79c46f23d5dea4a97ece3bc2f9f0ca9c3ff013727c22736b.png',
)
EXAMPLE_DESCRIPTION = '''# blender-kitsu
blender-kitsu is a Blender Add-on to interact with Kitsu from within Blender.
It also has features that are not directly related to Kitsu but support certain
aspects of the Blender Studio Pipeline.

## Table of Contents
- [Installation](#installation)
- [How to get started](#how-to-get-started)
- [Features](#features)
    - [Sequence Editor](#sequence-editor)
    - [Context](#context)
- [Troubleshoot](#troubleshoot)

## Installation
Download or clone this repository.
In the root project folder you will find the 'blender_kitsu' folder.
Place this folder in your Blender addons directory or create a sym link to it.

## How to get started
After installing you need to setup the addon preferences to fit your environment.
In order to be able to log in to Kitsu you need a server that runs
the Kitsu production management suite.
Information on how to set up Kitsu can be found [here](https://zou.cg-wire.com/).

If Kitsu is up and running and you can succesfully log in via the web interface you have
to setup the `addon preferences`.

...
'''
LICENSES = (LICENSE_GPL2.id, LICENSE_GPL3.id)


class Command(BaseCommand):
    help = 'Generate fake data with extensions, users and versions using test factories.'

    @transaction.atomic
    def handle(self, *args, **options):
        verbosity = int(options['verbosity'])
        root_logger = logging.getLogger('root')
        if verbosity > 2:
            root_logger.setLevel(logging.DEBUG)
        elif verbosity > 1:
            root_logger.setLevel(logging.INFO)
        else:
            root_logger.setLevel(logging.WARNING)

        tags = {
            type_id: list(Tag.objects.filter(type=type_id).values_list('name', flat=True))
            for type_id, _ in Extension.TYPES
        }

        # Create a fixed example
        example_version = create_approved_version(
            file=FileFactory(
                metadata__blender_version_min='2.93.0',
                metadata__id='blender_kitsu',
                metadata__name='Blender Kitsu',
                metadata__support='https://developer.blender.org/',
                metadata__tags=['Development'],
                metadata__version='0.1.5-alpha+f52258de',
                metadata__website='https://studio.blender.org/',
                original_hash=FILE_SOURCES["blender-kitsu"]["hash"],
                size_bytes=FILE_SOURCES["blender-kitsu"]["size"],
                source=FILE_SOURCES["blender-kitsu"]["file"],
                status=File.STATUSES.APPROVED,
            ),
        )
        for preview in [
            ImageFactory(source=source, status=File.STATUSES.APPROVED)
            for source in PREVIEW_SOURCES[-2:]
        ]:
            example_version.extension.previews.add(preview)
        example_version.permissions.add(VERSION_PERMISSION_FILE.id)
        example_version.permissions.add(VERSION_PERMISSION_NETWORK.id)
        example_version.licenses.add(LICENSE_GPL2.id)

        # Create a few publicly listed extensions
        for i in range(10):
            type = random.choice(Extension.TYPES)[0]
            version = create_approved_version(
                metadata__tags=random.sample(tags[type], k=1),
                status=File.STATUSES.APPROVED,
                type=type,
            )
            for preview in [
                ImageFactory(source=source, status=File.STATUSES.APPROVED)
                for source in random.sample(
                    PREVIEW_SOURCES, k=random.randint(1, len(PREVIEW_SOURCES) - 1)
                )
            ]:
                version.extension.previews.add(preview)
            for i in range(random.randint(1, len(LICENSES))):
                version.licenses.add(LICENSES[i])

        # Create a few unlisted extension versions
        for i in range(5):
            type = random.choice(Extension.TYPES)[0]
            version = create_version(
                metadata__tags=random.sample(tags[type], k=1),
                status=random.choice((File.STATUSES.DISABLED, File.STATUSES.DISABLED_BY_AUTHOR)),
                type=type,
            )
            for i in range(random.randint(1, len(LICENSES))):
                version.licenses.add(LICENSES[i])

        example_version.extension.average_score = 5.0
        example_version.extension.save(update_fields={'average_score'})
        example_author = example_version.extension.authors.first()
        example_author.username = 'PaulGolter'
        example_author.full_name = 'Paul Golter'
        example_author.email = 'paul.golter@example.com'
        example_author.save(update_fields={'username', 'full_name', 'email'})
        example_team = TeamFactory(name='Blender Studio')
        example_version.extension.team = example_team
        example_version.extension.save(update_fields={'team'})
