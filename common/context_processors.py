"""Various additions to template context.

**N.B.**: DO NOT ever expose Django settings object in the template context: it's a security risk!
"""
from typing import Dict

from django.conf import settings
from django.http.request import HttpRequest


def extra_context(request: HttpRequest) -> Dict[str, str]:
    """Injects some configuration values into template context."""
    user_is_moderator = False
    if request.user.is_authenticated:
        user_is_moderator = request.user.is_moderator
    return {
        'BLENDER_ID': {
            'BASE_URL': settings.BLENDER_ID['BASE_URL'],
        },
        'canonical_url': request.build_absolute_uri(request.path),
        'root_url': request.build_absolute_uri('/'),
        'user_is_moderator': user_is_moderator,
    }
