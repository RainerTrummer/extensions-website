from urllib.parse import urlparse
import json
import logging

from django.forms.boundfield import BoundField
from django.template import Library, loader
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

from markupsafe import Markup

from common.markdown import (
    render as render_markdown,
    render_as_text as render_markdown_as_text,
)
from extensions.models import Tag
from notifications.models import Notification

import utils

register = Library()
logger = logging.getLogger(__name__)


@register.simple_tag(takes_context=True)
def absolute_url(context, path: str) -> str:
    """Return an absolute URL of a given path."""
    request = context.get('request')
    return utils.absolutify(path, request=request)


# Allows for example to go to another page of search
# results while keeping the search query.
# Credit: https://stackoverflow.com/questions/46026268/
@register.simple_tag(takes_context=True)
def query_transform(context, **kwargs):
    query = context['request'].GET.copy()
    for k, v in kwargs.items():
        query[k] = v
    return query.urlencode()


class PaginationRenderer:
    def __init__(self, pager):
        self.pager = pager

        self.max = 10
        self.span = (self.max - 1) // 2

        self.page = pager.number
        self.num_pages = pager.paginator.num_pages
        self.count = pager.paginator.count

        pager.page_range = self.range()
        pager.dotted_upper = self.num_pages not in pager.page_range
        pager.dotted_lower = 1 not in pager.page_range

    def range(self):
        """Return a list of page numbers to show in the paginator."""
        page, total, span = self.page, self.num_pages, self.span
        if total < self.max:
            lower, upper = 0, total
        elif page < span + 1:
            lower, upper = 0, span * 2
        elif page > total - span:
            lower, upper = total - span * 2, total
        else:
            lower, upper = page - span, page + span - 1
        return range(max(lower + 1, 1), min(total, upper) + 1)

    def render(self):
        c = {'pager': self.pager, 'num_pages': self.num_pages, 'count': self.count}
        t = loader.get_template('common/paginator.html').render(c)
        return Markup(t)


@register.filter(needs_autoescape=True)
def paginator(pager, autoescape=True):
    return mark_safe(PaginationRenderer(pager).render())


@register.filter
def urlparams(url, page, *args, **kwargs):
    return Markup(utils.urlparams(url, page, *args, **kwargs))


@register.filter
def class_selected(a, b):
    """Return ``'class="selected"'`` if ``a == b``."""
    return mark_safe('active' if a == b else '')


@register.simple_tag
def get_proper_elided_page_range(page_obj):
    paginator = page_obj.paginator
    return paginator.get_elided_page_range(number=page_obj.number)


@register.filter(name='add_form_classes')
def add_form_classes(form, size_class=""):
    """Add Bootstrap classes and our custom classes to the form fields."""
    for field_name, field in form.fields.items():
        input_type = getattr(field.widget, 'input_type', None)
        if input_type in ('radio',):
            continue
        classes = {'form-check-input'} if input_type in ('checkbox',) else {'form-control'}
        if size_class:
            classes.add(f'form-control-{size_class}')
        if input_type == 'select':
            classes.add('form-select')
            if size_class:
                classes.add(f'form-select-{size_class}')
        if field_name == 'tags':
            field.widget.attrs['data-tag-list'] = json.dumps(
                [tag.name for tag in Tag.objects.all()]
            )
        field.widget.attrs.update({'class': ' '.join(classes)})

    # Add error class to all the fields with errors
    invalid_fields = form.fields if '__all__' in form.errors else form.errors
    for field_name in invalid_fields:
        attrs = form.fields[field_name].widget.attrs
        attrs.update({'class': attrs.get('class', '') + ' is-invalid'})
    return form


@register.filter(name='markdown')
def markdown(text: str) -> Markup:
    """Render markdown."""
    return render_markdown(text)


@register.filter(name='unmarkdown')
def unmarkdown(text: str) -> str:
    """Remove markdown from markdown, leave text."""
    try:
        return render_markdown_as_text(text)
    except Exception:
        logger.exception('Failed to render markdown "as text"')
        return text


@register.filter(name='domain_from_url')
@stringfilter
def domain_from_url(value: str) -> str:
    value = value.replace('//www.', '//')
    return urlparse(value).hostname


@register.filter(name='path_from_url')
@stringfilter
def path_from_url(url: str) -> str:
    url = urlparse(url)
    domain_without_www = url.hostname.replace("www.", "")
    path = url.path
    if path.endswith("/"):
        path = path[:-1]

    return f"{domain_without_www}{path}"


@register.filter(name='version_without_patch')
@stringfilter
def version_without_patch(version_string: str) -> str:
    """Return the version without the patch part. (ex: 4.2.0 becomes 4.2)"""
    version_parts = version_string.split('.')
    version_without_patch = '.'.join(version_parts[:2])
    return version_without_patch


@register.filter(name='replace')
def replace(value, old_char_new_char):
    """Replaces occurrences of old_char with new_char in the given value."""
    old_char, new_char = old_char_new_char.split(',')
    return value.replace(old_char, new_char)


@register.filter(name='unread_notification_count')
def unread_notification_count(user):
    return Notification.objects.filter(recipient=user, read_at__isnull=True).count()


@register.filter(name='add_classes')
def add_classes(bound_field: BoundField, classes: str):
    """Add classes to the `class` attribute of the given form field's widget.

    Expects a string of space-separated classes.
    """
    class_value = bound_field.field.widget.attrs.get('class', '')
    bound_field.field.widget.attrs['class'] = class_value + f' {classes}'
    return bound_field


@register.filter(name='set_placeholder')
def set_placeholder(bound_field: BoundField, placeholder: str):
    """Set `placeholder` attribute of the given form field's widget."""
    bound_field.field.widget.attrs['placeholder'] = placeholder
    return bound_field


@register.filter(name='remove_cols_rows')
def remove_cols_rows(bound_field: BoundField):
    """Removes cols and rows attributes from the form field's widget.

    We'd rather go the CSS route when it comes to styling textareas.
    """
    bound_field.field.widget.attrs.pop('cols', None)
    bound_field.field.widget.attrs.pop('rows', None)
    return bound_field

@register.filter
def get_nth(value, n):
    """Gets the nth element of a list, where n is a 0-based index."""
    try:
        # Keep n as 0-based index and return the nth element
        return value[int(n)]
    except (IndexError, ValueError, TypeError):
        return None

@register.filter
@stringfilter
def split(value, key):
    """Splits the value by a key and returns a list of parts."""
    return value.split(key)

@register.filter
def to_int(value):
    """Convert a string to an integer."""
    try:
        return int(value)
    except (ValueError, TypeError):
        return 0
