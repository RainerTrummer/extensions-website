import random

from factory.django import DjangoModelFactory
from faker import Faker
from mdgen import MarkdownPostProvider
import factory
import factory.fuzzy

from common.tests.factories.files import FileFactory
from extensions.models import Extension, Version, Preview
from ratings.models import Rating

fake_markdown = Faker()
fake_markdown.add_provider(MarkdownPostProvider)


class ExtensionFactory(DjangoModelFactory):
    class Meta:
        model = Extension

    name = factory.Faker('catch_phrase')
    extension_id = factory.Faker('slug')
    slug = factory.Faker('slug')
    description = factory.LazyAttribute(
        lambda _: fake_markdown.post(size=random.choice(('medium', 'large')))
    )
    support = factory.Faker('url')
    website = factory.Faker('url')

    download_count = factory.Faker('random_int')
    view_count = factory.Faker('random_int')

    @factory.post_generation
    def previews(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for _ in extracted:
                Preview.objects.create(file=_, caption='Media Caption', extension=self)

    @factory.post_generation
    def process_extension_id(self, created, extracted, **kwargs):
        self.extension_id = self.extension_id.replace("-", "_")


class RatingFactory(DjangoModelFactory):
    class Meta:
        model = Rating

    score = factory.fuzzy.FuzzyChoice(choices=Rating.SCORES.values)
    text = factory.Faker('text')
    ip_address = factory.Faker('ipv4_private')
    status = factory.fuzzy.FuzzyChoice(choices=Rating.STATUSES.values)

    user = factory.SubFactory('common.tests.factories.users.UserFactory')
    extension = factory.LazyAttribute(lambda o: o.version.extension)


def create_version(**kwargs) -> 'Version':
    extension = kwargs.pop('extension', None)
    file = kwargs.pop('file', None)

    if not file:
        file = FileFactory(**kwargs)

    if not extension:
        extension = Extension.create_from_file(file)

    return extension.create_version_from_file(file)


def create_approved_version(**kwargs) -> 'Version':
    version = create_version(**kwargs)
    version.extension.approve()
    version.refresh_from_db()
    return version
