from factory.django import DjangoModelFactory
import factory

import abuse.models


class AbuseReportFactory(DjangoModelFactory):
    class Meta:
        model = abuse.models.AbuseReport

    reporter = factory.SubFactory('common.tests.factories.users.UserFactory')
    user = factory.SubFactory('common.tests.factories.users.UserFactory')
