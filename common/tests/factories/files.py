import random

from factory.django import DjangoModelFactory
import factory
import factory.fuzzy

from files.models import File


class ManifestFactory(factory.DictFactory):
    name = factory.Faker('catch_phrase')
    id = factory.Faker('slug')
    support = factory.Faker('url')
    website = factory.Faker('url')
    version = factory.LazyAttribute(
        lambda _: f'{random.randint(0, 9)}.{random.randint(0, 9)}.{random.randint(0, 9)}',
    )
    blender_version_min = factory.fuzzy.FuzzyChoice(
        {'2.83.1', '2.93.0', '2.93.8', '3.0.0', '3.2.1'}
    )
    tagline = factory.Faker('bs')
    schema_version = '1.0.0'
    platforms = []
    tags = []


class FileFactory(DjangoModelFactory):
    class Meta:
        model = File

    hash = factory.Faker('lexify', text='fakehash:??????????????????', letters='deadbeef')
    metadata = factory.SubFactory(ManifestFactory)
    original_hash = factory.Faker('lexify', text='fakehash:??????????????????', letters='deadbeef')
    original_name = factory.LazyAttribute(lambda x: x.source)
    size_bytes = factory.Faker('random_int', min=1234)
    source = factory.Faker('file_name', extension='zip')
    type = File.TYPES.BPY
    user = factory.SubFactory('common.tests.factories.users.UserFactory')


class ImageFactory(FileFactory):
    metadata = {}
    original_name = factory.Faker('file_name', extension='png')
    size_bytes = 1234
    source = 'images/de/deadbeef.png'
    type = File.TYPES.IMAGE


class VideoFactory(FileFactory):
    metadata = {}
    original_name = factory.Faker('file_name', extension='mp4')
    size_bytes = 12345678
    source = 'images/be/beefcafe.mp4'
    type = File.TYPES.VIDEO
