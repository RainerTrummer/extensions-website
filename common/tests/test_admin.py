from django.test import TestCase

from common.tests.factories.users import UserFactory
import common.tests.utils
import utils

SKIP_PATHS = {'add/', 'login/', 'logout/', 'autocomplete/', '%'}


class AdminTest(TestCase):
    def setUp(self):
        admin_user = UserFactory(is_staff=True, is_superuser=True)
        self.client.force_login(admin_user)

    def _test_get_changelist(self, path):
        response = self.client.get(path)
        self.assertEqual(response.status_code, 200, path)


for entry in common.tests.utils.extract_urls():
    path = '/' + entry.normalize()[0][0]
    if not path.startswith('/admin/'):
        continue
    if any(_ in path for _ in SKIP_PATHS):
        continue

    def _create_test_method(path):
        def _method(self):
            self._test_get_changelist(path)

        return _method

    setattr(
        AdminTest,
        f'test_get_{utils.slugify(path).replace("-", "_")}',
        _create_test_method(path),
    )
