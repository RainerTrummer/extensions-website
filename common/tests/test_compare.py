from django.core.exceptions import ValidationError
from django.test import TestCase

import common.compare


class TestVersions(TestCase):
    def test_is_same_release(self):
        with self.assertRaises(ValueError):
            self.assertTrue(common.compare.is_same_release('asdfa.asdfasd', 'sss.asdfa.a111'))

        self.assertTrue(common.compare.is_same_release('2.83.3', '2.83.1'))
        self.assertTrue(common.compare.is_same_release('3.3.11', '3.3.0'))

        self.assertFalse(common.compare.is_same_release('2.83.3', '2.93.0'))
        self.assertFalse(common.compare.is_same_release('2.83.1', '2.93.1'))
        self.assertFalse(common.compare.is_same_release('3.2.11', '3.3.0'))

    def test_migration_semantic_version(self):
        self.assertEqual('3.84.1', common.compare.migration_semantic_version('3.84.1'))
        self.assertEqual('2.49.0', common.compare.migration_semantic_version('2.49'))
        self.assertEqual('5.0.0', common.compare.migration_semantic_version('5'))
        self.assertEqual('37.0.0', common.compare.migration_semantic_version('37'))
        self.assertEqual('1.2.3+4', common.compare.migration_semantic_version('1.2.3.4'))
        self.assertEqual('2.84.0-beta', common.compare.migration_semantic_version('2.84-beta'))
        self.assertEqual('3.42.0-beta', common.compare.migration_semantic_version('3.42beta'))
        with self.assertRaises(TypeError):
            common.compare.migration_semantic_version(None)
        self.assertEqual('0.1.0', common.compare.migration_semantic_version('foo.bar'))

    def test_version(self):
        self.assertEqual('2.4.9', str(common.compare.version('2.4.9', coerce=True)))
        with self.assertRaises(ValidationError):
            common.compare.version(None)
        with self.assertRaises(ValidationError):
            common.compare.version("foo.bar")

    def test_is_in_version_range(self):
        self.assertTrue(common.compare.is_in_version_range('2.93.0', '2.92.2'))
        self.assertTrue(common.compare.is_in_version_range('3.0.0', '3.0.0', "3.0.1"))
        self.assertFalse(common.compare.is_in_version_range('3.0.1', '3.0.0', "3.0.1"))
        self.assertFalse(common.compare.is_in_version_range('3.0.1', '3.0.1', "3.0.1"))
        self.assertFalse(common.compare.is_in_version_range('3.0.1', '3.0.2', "3.0.1"))
        self.assertFalse(common.compare.is_in_version_range('3.0.1', '3.0.1', "3.0.0"))
        self.assertFalse(common.compare.is_in_version_range('2.93.0-beta', '2.93.0'))
        self.assertTrue(common.compare.is_in_version_range('2.93.0-beta', '2.92.0', '2.93.0'))
        self.assertTrue(common.compare.is_in_version_range('4.0.0', '4.0.0-beta', '4.0.1'))
        self.assertFalse(common.compare.is_in_version_range('4.0.0', '4.0.0-beta', '4.0.0-beta'))
