import contextlib
import threading

_locals = threading.local()
_locals.user = None
_locals.remote_addr = None


def get_remote_addr():
    return getattr(_locals, 'remote_addr', None)


def set_remote_addr(remote_addr):
    _locals.remote_addr = remote_addr


@contextlib.contextmanager
def override_remote_addr(remote_addr_override):
    """Override value returned by get_remote_addr() for a specific context."""
    original = get_remote_addr()
    set_remote_addr(remote_addr_override)
    yield
    set_remote_addr(original)
