from django.test import TestCase
from django.urls import reverse


from common.tests.factories.extensions import create_version
from common.tests.factories.teams import TeamFactory
from common.tests.factories.users import UserFactory
from constants.base import TEAM_ROLE_MANAGER, TEAM_ROLE_MEMBER
from teams.models import TeamsUsers


class TeamLeaveTest(TestCase):
    def test_the_only_manager_cant_leave(self):
        team = TeamFactory(slug='test-team')
        user = UserFactory()
        TeamsUsers(team=team, user=user, role=TEAM_ROLE_MANAGER).save()
        self.assertEqual(user.teams.count(), 1)

        self.client.force_login(user)
        response = self.client.get(reverse('teams:leave-team', args=[team.slug]))
        self.assertContains(response, 'cannot leave')
        self.client.post(reverse('teams:leave-team', args=[team.slug]))
        user.refresh_from_db()
        self.assertEqual(user.teams.count(), 1)

        # create another manager
        user2 = UserFactory()
        TeamsUsers(team=team, user=user2, role=TEAM_ROLE_MANAGER).save()
        # try to leave again
        response = self.client.get(reverse('teams:leave-team', args=[team.slug]))
        self.assertNotContains(response, 'cannot leave')
        self.client.post(reverse('teams:leave-team', args=[team.slug]))
        user.refresh_from_db()
        self.assertEqual(user.teams.count(), 0)

    def test_extensions_lose_team_assignment(self):
        team = TeamFactory(slug='test-team')
        user = UserFactory()
        TeamsUsers(team=team, user=user, role=TEAM_ROLE_MEMBER).save()

        extension = create_version().extension
        extension.team = team
        extension.authors.add(user)
        extension.save()

        self.client.force_login(user)
        self.client.post(reverse('teams:leave-team', args=[team.slug]))
        user.refresh_from_db()
        self.assertEqual(user.teams.count(), 0)

        extension.refresh_from_db()
        self.assertIsNone(extension.team)
