from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

import common.help_texts
from common.model_mixins import CreatedModifiedMixin, RecordDeletionMixin

from constants.base import EXTENSION_TYPE_CHOICES
from constants.reviewers import CANNED_RESPONSE_CATEGORY_CHOICES

User = get_user_model()


class CannedResponse(CreatedModifiedMixin, models.Model):
    TYPES = EXTENSION_TYPE_CHOICES
    CATEGORIES = CANNED_RESPONSE_CATEGORY_CHOICES

    name = models.CharField(max_length=255)
    response = models.TextField()
    sort_group = models.CharField(max_length=255)
    type = models.PositiveIntegerField(choices=TYPES, db_index=True, default=TYPES.BPY)

    # Category is used only by code-manager
    category = models.PositiveIntegerField(choices=CATEGORIES, default=CATEGORIES.OTHER)

    def __str__(self):
        return str(self.name)


class ApprovalActivity(CreatedModifiedMixin, RecordDeletionMixin, models.Model):
    class ActivityType(models.TextChoices):
        COMMENT = "COM", _("Comment")
        APPROVED = "APR", _("Approved")
        AWAITING_CHANGES = "AWC", _("Awaiting Changes")
        AWAITING_REVIEW = "AWR", _("Awaiting Review")
        UPLOADED_NEW_VERSION = "UNV", _("Uploaded New Version")

    user = models.ForeignKey(User, on_delete=models.PROTECT, blank=True, null=True)
    extension = models.ForeignKey(
        'extensions.Extension',
        on_delete=models.CASCADE,
        related_name='review_activity',
    )
    type = models.CharField(
        max_length=3,
        choices=ActivityType.choices,
        default=ActivityType.COMMENT,
    )
    message = models.TextField(help_text=common.help_texts.markdown, blank=False, null=False)

    class Meta:
        verbose_name_plural = "Review activity"

    def __str__(self):
        return f"{self.extension.name}: {self.get_type_display()}"
