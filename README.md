# Blender Extensions

Free and open source add-ons and themes for Blender.

* [Requirements](#requirements)
  * [Development](#development)
      * [Extra requirements](#extra-requirements)
      * [Setup](#setup)
      * [Blender ID](#blender-id)
  * [Pre-commit hooks](#pre-commit-hooks)
  * [Testing](#testing)
* [Deploy](#deploy)
* [License and Copyright](#license-and-copyright)


# Requirements

* Python 3.10
* `libmagic`: `sudo apt-get install libmagic1` in Debian/Ubuntu, `brew install libmagic` on OSX.


## Development

### Extra requirements

* `virtualenv`

### Setup

Checkout Blender Web Assets submodule first:

    git submodule update --init --recursive

Create and activate a virtual environment using your favourite method, e.g.

    virtualenv .venv -p python
    source .venv/bin/activate

Install required packages:

    pip install -r requirements_dev.txt

Create the database tables and load some basic data to work with using the following commands:

    ./manage.py migrate
    ./manage.py loaddata **/fixtures/*.json

It's also possible to generate fake add-on data using the following command:

    ./manage.py generate_fake_data

Run development server

    ./manage.py runserver 8111

Update `/etc/hosts` to point to `extensions.local`, e.g.:

    127.0.0.1	extensions.local

Now http://extensions.local:8111 should be ready to work with and it should be possible to
log into http://extensions.local:8111/admin/ with `admin`/`admin`.

### Blender ID

Blender Extensions, as all other Blender web services, uses Blender ID.
Blender Extensions can also receive Blender ID account modifications such as badge updates
via a webhook.

For development, Blender ID's code contains a fixture with an OAuth app and a webhook
that should work without any changes to default configuration.
To load this fixture, go to your development Blender ID and run the following:

    ./manage.py loaddata blender_extensions_devserver

**N.B.**: the webhook view delegates the actual updating of the user profile
to a background task, so in order to see the updates locally, start the processing of
tasks using the following:

    ./manage.py process_tasks

#### Blender ID and staging/production

For staging/production, create an OAuth2 application in Blender ID using
Admin › Blender-ID › OAuth2 applications -> Add:

* Redirect URIs: `https://staging.extensions.blender.org/oauth/authorized` (`https://extensions.blender.org` for production);
* Client type: "Confidential";
* Authorization grant type: "Authorization code";
* Name: "Blender Extensions Staging" (or "Blender Extensions" for production);

Copy client ID and secret and save them as `BID_OAUTH_CLIENT` and `BID_OAUTH_SECRET` into a `.env` file:

    export BID_OAUTH_CLIENT=<CLIENT ID HERE>
    export BID_OAUTH_SECRET=<SECRET HERE>

Create a webhook using Admin › Blender-ID API › Webhooks > Add:

* Name: "Blender Extensions Staging" (or "Blender Extensions" for production)";
* URL: `https://staging.extensions.blender.org/webhooks/user-modified/` (or `https://extensions.blender.org/webhooks/user-modified/` for production);
* App: choose the app created in the previous step;

Copy webhook's secret into the `.env` file as `BID_WEBHOOK_USER_MODIFIED_SECRET`:

    export BID_WEBHOOK_USER_MODIFIED_SECRET=<WEBHOOK SECRET HERE>

## Pre-commit hooks

Make sure to enable pre-commit hooks after installing requirements from `requirements_dev.txt`:

    pre-commit install

This will enable formatting pre-commit checks for Django templates, Python and JS modules.

## Testing

To simply run the test suit use

    ./manage.py test

To run tests and generate a coverage report use

    coverage run manage.py test && coverage html

and then open `htmlcov/index.html` with your favourite browser.

# Deploy

See [playbooks](playbooks#deploy).

# Feature Flags

At the moment there are the following feature flags:
* `is_alpha`
* `is_beta`

To create them run

    ./manage.py waffle_switch --create is_alpha on

    ./manage.py waffle_switch --create is_beta off

They can optionally be setup on the deployment platform to set the release stage of the site.
These settings will be removed once the site is officially launched.

# License and Copyright

The platform source code is available under the GPLv3 or later. Early versions of the platform
are inspired by [Mozilla Addons Server](https://github.com/mozilla/addons-server). 
List of authors is available in the AUTHORS file.
