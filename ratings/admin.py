from django.contrib import admin
from django.db.models import Prefetch
from django.template.defaultfilters import truncatechars

from extensions.models import Version

from .models import Rating


class RatingAdmin(admin.ModelAdmin):
    date_hierarchy = 'date_created'
    search_fields = ('extension__slug', 'extension__name', 'user__email', 'user__full_name')
    raw_id_fields = (
        'extension',
        'version',
        'user',
    )
    readonly_fields = (
        'date_created',
        'date_modified',
        'extension',
        'version',
        'text',
        'score',
        'user',
    )
    fields = ('status',) + readonly_fields
    list_display = (
        'date_created',
        'extension',
        'user',
        'ip_address',
        'score',
        'status',
        'truncated_text',
    )
    list_filter = ('status', 'score')
    actions = ('delete_selected',)
    list_select_related = ('user',)

    def get_queryset(self, request):
        base_qs = Rating.objects.all()
        return base_qs.prefetch_related(
            Prefetch('version', queryset=Version.objects.all()),
        )

    def has_add_permission(self, request):
        return False

    def truncated_text(self, obj):
        return truncatechars(obj.text, 140) if obj.text else ''


admin.site.register(Rating, RatingAdmin)
