from django import forms

from ratings.models import Rating


class AddRatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ['score', 'text', 'version']

    def __init__(self, *args, **kwargs):
        extension = kwargs.pop('extension')
        super().__init__(*args, **kwargs)
        self.fields['version'].label_from_instance = lambda v: v.version
        queryset = extension.versions.listed
        self.fields['version'].queryset = queryset
        if queryset.count() == 1:
            self.fields['version'].initial = extension.latest_version
            self.fields['version'].widget = forms.HiddenInput()
