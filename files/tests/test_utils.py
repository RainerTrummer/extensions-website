from pathlib import Path
from unittest.mock import patch, ANY
import dataclasses
import tempfile

from django.test import TestCase

from files.utils import (
    extract_frame,
    filter_paths_by_ext,
    find_exact_path,
    find_path_by_name,
    get_thumbnail_upload_to,
    make_thumbnails,
    validate_file_list,
)

# Reusing test files from the extensions app
TEST_FILES_DIR = Path(__file__).resolve().parent.parent.parent / 'extensions' / 'tests' / 'files'


@patch(
    'django.core.files.storage.base.Storage.get_available_name',
    # Make storage return the path with a non-random suffix
    lambda _, name, max_length=None: '{}_random7.{}'.format(*name.split('.')),
)
@patch(
    'django.core.files.storage.filesystem.FileSystemStorage._save',
    # Skip the actual writing of the file
    lambda _, name, __: name,
)
class UtilsTest(TestCase):
    manifest = 'blender_manifest.toml'

    def test_find_manifest(self):
        name_list = [
            "blender_manifest.toml",
        ]
        manifest_file = find_path_by_name(name_list, self.manifest)
        self.assertEqual(manifest_file, "blender_manifest.toml")

    def test_find_manifest_nested(self):
        name_list = [
            "foobar-1.0.3/",
            "foobar-1.0.3/bar.txt",
            "foobar-1.0.3/foo.txt",
            "foobar-1.0.3/another_manifest.toml",
            "foobar-1.0.3/blender_manifest.toml",
            "foobar-1.0.3/manifest.toml",
            "foobar-1.0.3/manifest.json",
        ]
        manifest_file = find_path_by_name(name_list, self.manifest)
        self.assertEqual(manifest_file, "foobar-1.0.3/blender_manifest.toml")

    def test_find_manifest_no_zipped_folder(self):
        name_list = [
            "foobar-1.0.3/blender_manifest.toml",
        ]
        manifest_file = find_path_by_name(name_list, self.manifest)
        self.assertEqual(manifest_file, "foobar-1.0.3/blender_manifest.toml")

    def test_find_manifest_no_manifest(self):
        name_list = [
            "foobar-1.0.3/",
        ]
        manifest_file = find_path_by_name(name_list, self.manifest)
        self.assertEqual(manifest_file, None)

    def test_find_manifest_with_space(self):
        name_list = [
            "foobar-1.0.3/ blender_manifest.toml",
            "foobar-1.0.3/_blender_manifest.toml",
            "foobar-1.0.3/blender_manifest.toml.txt",
            "blender_manifest.toml/my_files.py",
        ]
        manifest_file = find_path_by_name(name_list, self.manifest)
        self.assertEqual(manifest_file, None)

    def test_find_exact_path_found(self):
        name_list = [
            'foobar-1.0.3/theme.xml',
            'foobar-1.0.3/theme1.xml',
            'foobar-1.0.3/theme2.txt',
            'foobar-1.0.3/__init__.py',
            'foobar-1.0.3/foobar/__init__.py',
            'foobar-1.0.3/foobar-1.0.3/__init__.py',
            'blender_manifest.toml',
        ]
        path = find_exact_path(name_list, 'foobar-1.0.3/__init__.py')
        self.assertEqual(path, 'foobar-1.0.3/__init__.py')

    def test_find_exact_path_nothing_found(self):
        name_list = [
            'foobar-1.0.3/theme.xml',
            'foobar-1.0.3/theme1.xml',
            'foobar-1.0.3/theme2.txt',
            'foobar-1.0.3/foobar/__init__.py',
            'foobar-1.0.3/foobar-1.0.3/__init__.py',
            'blender_manifest.toml',
        ]
        path = find_exact_path(name_list, 'foobar-1.0.3/__init__.py')
        self.assertIsNone(path)

    def test_filter_paths_by_ext_found(self):
        name_list = [
            'foobar-1.0.3/theme.xml',
            'foobar-1.0.3/theme1.xml',
            'foobar-1.0.3/theme2.txt',
            'foobar-1.0.3/__init__.py',
            'foobar-1.0.3/foobar-1.0.3/__init__.py',
            'blender_manifest.toml',
        ]
        paths = filter_paths_by_ext(name_list, '.xml')
        self.assertEqual(list(paths), ['foobar-1.0.3/theme.xml', 'foobar-1.0.3/theme1.xml'])

    def test_filter_paths_by_ext_nothing_found(self):
        name_list = [
            'foobar-1.0.3/theme.xml',
            'foobar-1.0.3/theme1.md.xml',
            'foobar-1.0.3/theme2.txt',
            'foobar-1.0.3/__init__.py',
            'foobar-1.0.3/foobar-1.0.3/__init__.py',
            'blender_manifest.toml',
        ]
        paths = filter_paths_by_ext(name_list, '.md')
        self.assertEqual(list(paths), [])

    def test_get_thumbnail_upload_to(self):
        for file_hash, kwargs, expected in (
            ('foobar', {}, 'thumbnails/fo/foobar.webp'),
            ('deadbeef', {'width': None, 'height': None}, 'thumbnails/de/deadbeef.webp'),
            ('deadbeef', {'width': 640, 'height': 360}, 'thumbnails/de/deadbeef_640x360.webp'),
        ):
            with self.subTest(file_hash=file_hash, kwargs=kwargs):
                self.assertEqual(get_thumbnail_upload_to(file_hash, **kwargs), expected)

    @patch('files.utils.resize_image')
    def test_make_thumbnails(self, mock_resize_image):
        self.assertEqual(
            {
                '1080p': {
                    'path': 'thumbnails/fo/foobar_1920x1080_random7.webp',
                    'size': [1920, 1080],
                },
                '360p': {'path': 'thumbnails/fo/foobar_640x360_random7.webp', 'size': [640, 360]},
            },
            make_thumbnails(TEST_FILES_DIR / 'test_preview_image_0001.png', 'foobar'),
        )

        self.assertEqual(len(mock_resize_image.mock_calls), 2)
        for expected_size in ([1920, 1080], [640, 360]):
            with self.subTest(expected_size=expected_size):
                mock_resize_image.assert_any_call(
                    ANY,
                    expected_size,
                    ANY,
                    output_format='WEBP',
                    quality=83,
                    optimize=True,
                    progressive=True,
                )

    @patch('files.utils.FFmpeg')
    def test_extract_frame(self, mock_ffmpeg):
        with tempfile.TemporaryDirectory() as output_dir:
            extract_frame('path/to/source/video.mp4', output_dir + '/frame.png')
            mock_ffmpeg.return_value.option.return_value.input.return_value.output.assert_any_call(
                output_dir + '/frame.png', {'ss': '00:00:00.01', 'frames:v': 1, 'update': 'true'}
            )

        self.assertEqual(len(mock_ffmpeg.mock_calls), 5)
        mock_ffmpeg.assert_any_call()
        mock_ffmpeg.return_value.option.return_value.input.assert_any_call(
            'path/to/source/video.mp4'
        )

    def test_validate_file_list(self):
        @dataclasses.dataclass
        class TestParams:
            name: str
            toml_content: dict
            manifest_filepath: dict
            file_list: list
            expected: list

        for test in [
            TestParams(
                name='valid add-on',
                toml_content={'type': 'add-on'},
                manifest_filepath='blender_manifest.toml',
                file_list=['__init__.py'],
                expected=[],
            ),
            TestParams(
                name='add-on missing init',
                toml_content={'type': 'add-on'},
                manifest_filepath='blender_manifest.toml',
                file_list=[],
                expected=['invalid_missing_init'],
            ),
            TestParams(
                name='valid theme',
                toml_content={'type': 'theme'},
                manifest_filepath='blender_manifest.toml',
                file_list=['theme.xml'],
                expected=[],
            ),
            TestParams(
                name='missing theme file',
                toml_content={'type': 'theme'},
                manifest_filepath='blender_manifest.toml',
                file_list=[],
                expected=['missing_or_multiple_theme_xml'],
            ),
            TestParams(
                name='multiple theme files',
                toml_content={'type': 'theme'},
                manifest_filepath='blender_manifest.toml',
                file_list=['theme.xml', 'theme/2.xml'],
                expected=['missing_or_multiple_theme_xml'],
            ),
            TestParams(
                name='valid nested wheels',
                toml_content={'type': 'add-on', 'wheels': ['wheels/1.whl']},
                manifest_filepath='addon/blender_manifest.toml',
                file_list=['addon/__init__.py', 'addon/wheels/1.whl'],
                expected=[],
            ),
            TestParams(
                name='valid nested wheels with ./',
                toml_content={'type': 'add-on', 'wheels': ['./wheels/1.whl']},
                manifest_filepath='blender_manifest.toml',
                file_list=['__init__.py', 'wheels/1.whl'],
                expected=[],
            ),
            TestParams(
                name='missing wheel',
                toml_content={'type': 'add-on', 'wheels': ['./wheels/1.whl', './wheels/2.whl']},
                manifest_filepath='blender_manifest.toml',
                file_list=['__init__.py', 'wheels/1.whl'],
                expected=[{'code': 'missing_wheel', 'params': {'path': 'wheels/2.whl'}}],
            ),
            TestParams(
                name='incorrect wheel nesting',
                toml_content={'type': 'add-on', 'wheels': ['./wheels/1.whl']},
                manifest_filepath='add-on/blender_manifest.toml',
                file_list=['add-on/__init__.py', 'wheels/1.whl'],
                expected=[{'code': 'missing_wheel', 'params': {'path': 'add-on/wheels/1.whl'}}],
            ),
            TestParams(
                name='contains forbidden files at top level',
                toml_content={'type': 'add-on', 'wheels': ['./wheels/1.whl']},
                manifest_filepath='blender_manifest.toml',
                file_list=['__init__.py', 'wheels/1.whl', '__MACOSX/.__init__.py'],
                expected=[{'code': 'forbidden_filepaths', 'params': {'paths': '__MACOSX/'}}],
            ),
            TestParams(
                name='contains forbidden files in nested dir',
                toml_content={'type': 'add-on', 'wheels': ['./wheels/1.whl']},
                manifest_filepath='addon/blender_manifest.toml',
                file_list=['addon/__init__.py', 'addon/wheels/1.whl', 'addon/.git/config'],
                expected=[{'code': 'forbidden_filepaths', 'params': {'paths': '.git/'}}],
            ),
        ]:
            with self.subTest(**dataclasses.asdict(test)):
                self.assertEqual(
                    test.expected,
                    validate_file_list(test.toml_content, test.manifest_filepath, test.file_list),
                    test.name,
                )
