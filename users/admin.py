from django.contrib import admin
from django.contrib.auth import get_user_model, admin as auth_admin
from django.utils.translation import gettext_lazy as _


@admin.register(get_user_model())
class UserAdmin(auth_admin.UserAdmin):
    change_form_template = 'loginas/change_form.html'

    def has_add_permission(self, request):
        """User records are managed by Blender ID, so no new user should be added here."""
        return False

    list_display_links = ['username']
    list_filter = auth_admin.UserAdmin.list_filter + (
        'groups__name',
        'date_joined',
        'confirmed_email_at',
        'date_deletion_requested',
        'last_login',
    )

    list_display = [
        _ for _ in auth_admin.UserAdmin.list_display if _ not in ('first_name', 'last_name')
    ] + ['is_active', 'email_confirmed', 'deletion_requested']
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (
            _('Personal info'),
            {
                'fields': (
                    'full_name',
                    'image',
                    'email',
                    'badges',
                    'is_subscribed_to_notification_emails',
                )
            },
        ),
        (
            _('Permissions'),
            {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')},
        ),
        (
            _('Important dates'),
            {
                'fields': (
                    ('date_joined', 'last_login'),
                    'confirmed_email_at',
                    'date_deletion_requested',
                )
            },
        ),
    )
    readonly_fields = (
        'confirmed_email_at',
        'date_deletion_requested',
        'date_joined',
        'last_login',
    )
    ordering = ['-date_joined']
    search_fields = ['email', 'full_name', 'username']

    def deletion_requested(self, obj):
        """Display yes/no icon status of deletion request."""
        return obj.date_deletion_requested is not None

    deletion_requested.boolean = True

    def email_confirmed(self, obj):
        """Display yes/no icon status of email confirmation."""
        return obj.confirmed_email_at is not None

    email_confirmed.boolean = True
