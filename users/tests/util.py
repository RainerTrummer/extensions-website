from django.conf import settings
from django.contrib.auth import get_user_model
import responses

User = get_user_model()


def mock_blender_id_responses() -> None:
    """Set up mock responses of Blender ID service."""
    base_url = settings.BLENDER_ID['BASE_URL']
    responses.add(
        responses.GET,
        f'{base_url}api/badges/2',
        status=200,
        json={
            'user_id': 2,
            'badges': {
                'cloud_demo': {
                    'label': 'Blender Studio',
                    'description': 'Blender Studio free account',
                    'image': f'{base_url}media/badges/badge_cloud.png',
                    'image_width': 256,
                    'image_height': 256,
                },
            },
        },
    )
    responses.add(
        responses.GET,
        f'{base_url}api/me',
        json={
            'id': 2,
            'full_name': 'ⅉane ⅅoe',
            'email': 'jane@example.com',
            'nickname': 'ⅉanedoe',
            # N.B.: roles format here differs from one in user-modified webhook payload.
            'roles': {
                'dev_core': True,
                'cloud_has_subscription': True,
                'cloud_subscriber': True,
            },
        },
    )
    with open('common/static/common/images/blank-profile-pic.png', 'rb') as out:
        responses.add(
            responses.GET,
            f'{base_url}media/avatar/fo/ob/foobar_128x128.jpg',
            body=out,
            stream=True,
        )
