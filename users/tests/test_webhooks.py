from typing import Dict, Union
from unittest.mock import patch
import hashlib
import hmac
import json
import responses

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings, TransactionTestCase
from django.urls import reverse
import dateutil.parser

from common.tests.factories.users import OAuthUserFactory
import users.tests.util as util
import users.views.webhooks as webhooks

User = get_user_model()
BLENDER_ID_BASE_URL = settings.BLENDER_ID['BASE_URL']


def prepare_hmac_header(body: Union[str, dict], secret: str = 'testsecret') -> Dict[str, str]:
    """Return a dict containing an HMAC header matching a given request body."""
    if isinstance(body, dict):
        body = json.dumps(body).encode()

    mac = hmac.new(secret.encode(), body, hashlib.sha256)
    return {'HTTP_X-Webhook-HMAC': mac.hexdigest()}


@override_settings(
    BLENDER_ID={
        'BASE_URL': BLENDER_ID_BASE_URL,
        'OAUTH_CLIENT': 'testoauthclient',
        'OAUTH_SECRET': 'testoathsecret',
        'WEBHOOK_USER_MODIFIED_SECRET': b'testsecret',
    }
)
@patch(
    # Make sure background task is executed as a normal function
    'users.views.webhooks.handle_user_modified',
    new=webhooks.handle_user_modified.task_function,
)
class TestBlenderIDWebhook(TestCase):
    fixtures = ['dev']
    webhook_payload = {
        'avatar_changed': False,
        'avatar_url': None,
        'confirmed_email_at': None,
        'date_deletion_requested': None,
        'email': 'newmail@example.com',
        'full_name': 'Иван Васильевич Doe',
        'id': 2,
        'nickname': 'ivandoe',
        'old_email': 'mail@example.com',
        'old_nickname': 'old_ivandoe',
        'roles': [],
    }

    def setUp(self):
        self.url = reverse('users:webhook-user-modified')
        util.mock_blender_id_responses()

        # Prepare a user
        self.user = OAuthUserFactory(
            email='mail@example.com',
            username='very-original-username',
            oauth_info__oauth_user_id=2,
            oauth_tokens__oauth_user_id=2,
            oauth_tokens__access_token='testaccesstoken',
            oauth_tokens__refresh_token='testrefreshtoken',
        )

    def test_user_modified_missing_hmac(self):
        response = self.client.post(self.url, {}, content_type='application/json')

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'Invalid HMAC')

    def test_user_modified_invalid_hmac(self):
        url = reverse('users:webhook-user-modified')
        headers = {'HTTP_X-Webhook-HMAC': 'deadbeef'}
        response = self.client.post(url, {}, content_type='application/json', **headers)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'Invalid HMAC')

    @patch('users.views.webhooks.WEBHOOK_MAX_BODY_SIZE', 1)
    def test_user_modified_request_body_too_large(self):
        body = {"deadbeef": "foobar"}
        response = self.client.post(
            self.url, body, content_type='application/json', **prepare_hmac_header(body)
        )

        self.assertEqual(response.status_code, 413)

    def test_user_modified_unexpected_content_type(self):
        response = self.client.post(
            self.url, 'text', content_type='text/plain', **prepare_hmac_header(b'text')
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'Unsupported Content-Type')

    def test_user_modified_malformed_json(self):
        body = b'{"":"",}'
        response = self.client.post(
            self.url, body, content_type='application/json', **prepare_hmac_header(body)
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'Malformed JSON')

    @responses.activate
    def test_user_modified_updates_user(self):
        body = self.webhook_payload
        response = self.client.post(
            self.url, body, content_type='application/json', **prepare_hmac_header(body)
        )

        self.assertEqual(response.status_code, 204)
        self.assertEqual(response.content, b'')
        user = User.objects.get(id=self.user.pk)
        self.assertEqual(user.full_name, 'Иван Васильевич Doe')
        self.assertEqual(user.email, 'newmail@example.com')
        self.assertEqual(user.username, 'ivandoe')
        self.assertEqual(
            user.badges,
            {
                'cloud_demo': {
                    'description': 'Blender Studio free account',
                    'image': f'{BLENDER_ID_BASE_URL}media/badges/badge_cloud.png',
                    'image_height': 256,
                    'image_width': 256,
                    'label': 'Blender Studio',
                }
            },
        )

    def test_user_modified_missing_oauth_info(self):
        body = {
            **self.webhook_payload,
            'id': '999',
        }
        with self.assertLogs('users.views.webhooks', level='WARNING') as logs:
            response = self.client.post(
                self.url, body, content_type='application/json', **prepare_hmac_header(body)
            )
            self.assertRegex(
                logs.output[0], 'Skipping user-modified: no OAuth info found for ID 999'
            )

        self.assertEqual(response.status_code, 204)

    @responses.activate
    def test_user_modified_logs_errors_when_blender_id_badges_broken(self):
        body = self.webhook_payload
        # Mock a "broken" badges response
        responses.replace(
            responses.GET, f'{BLENDER_ID_BASE_URL}api/badges/2', status=403, body='Unauthorized'
        )
        response = self.client.post(
            self.url, body, content_type='application/json', **prepare_hmac_header(body)
        )
        self.assertEqual(response.status_code, 204)

    @responses.activate
    def test_user_modified_logs_errors_when_blender_id_avatar_url_broken(self):
        body = {
            **self.webhook_payload,
            'avatar_changed': True,
            'avatar_url': f'{BLENDER_ID_BASE_URL}media/avatar/fo/ob/foobar_128x128.jpg',
        }
        # Mock a "broken" response at `avatar_url`
        responses.replace(
            responses.GET, body['avatar_url'], status=500, body='Internal Server Error'
        )
        with self.assertLogs('users.blender_id', level='WARNING') as logs:
            response = self.client.post(
                self.url, body, content_type='application/json', **prepare_hmac_header(body)
            )
            self.assertRegex(logs.output[0], 'Failed to retrieve an image for')

        self.assertEqual(response.status_code, 204)

    @responses.activate
    def test_user_modified_avatar_changed(self):
        body = {
            **self.webhook_payload,
            'avatar_changed': True,
            'avatar_url': f'{BLENDER_ID_BASE_URL}media/avatar/fo/ob/foobar_128x128.jpg',
        }

        with self.assertLogs('users.blender_id', level='INFO') as logs:
            response = self.client.post(
                self.url, body, content_type='application/json', **prepare_hmac_header(body)
            )
            self.assertRegex(logs.output[0], 'Profile image updated for')

        self.assertEqual(response.status_code, 204)
        self.assertEqual(response.content, b'')
        user = User.objects.get(id=self.user.pk)
        self.assertTrue(user.image_url, 's3://file')

    @responses.activate
    def test_user_modified_date_deletion_requested_is_set(self):
        date_deletion_requested = '2020-12-31T23:02:03+00:00'
        body = {
            **self.webhook_payload,
            'date_deletion_requested': date_deletion_requested,
        }

        with self.assertLogs('users.models', level='WARNING') as logs:
            response = self.client.post(
                self.url, body, content_type='application/json', **prepare_hmac_header(body)
            )
            self.assertEqual(
                logs.output[0],
                f'WARNING:users.models:Deletion of pk={self.user.pk}'
                f' requested on {date_deletion_requested}, deactivating this account',
            )

        self.assertEqual(response.status_code, 204)
        self.assertEqual(response.content, b'')
        user = User.objects.get(id=self.user.pk)
        self.assertEqual(
            user.date_deletion_requested, dateutil.parser.parse(date_deletion_requested)
        )
        self.assertFalse(user.is_active)


@override_settings(
    BLENDER_ID={
        'BASE_URL': BLENDER_ID_BASE_URL,
        'OAUTH_CLIENT': 'testoauthclient',
        'OAUTH_SECRET': 'testoathsecret',
        'WEBHOOK_USER_MODIFIED_SECRET': b'testsecret',
    }
)
@patch(
    # Make sure background task is executed as a normal function
    'users.views.webhooks.handle_user_modified',
    new=webhooks.handle_user_modified.task_function,
)
class TestIntegrityErrors(TransactionTestCase):
    """Check that webhook handles cases that trigger `IntegrityError`s.

    In order to do that, it has to handle database transactions commits and rollbacks the same way
    "normal" code does, as opposed to how TestCase does it, otherwise it breaks test runs.
    See https://docs.djangoproject.com/en/3.0/topics/testing/tools/#django.test.TransactionTestCase
    """

    fixtures = ['dev']
    maxDiff = None
    webhook_payload = {
        'avatar_changed': False,
        'avatar_url': None,
        'confirmed_email_at': None,
        'date_deletion_requested': None,
        'email': 'newmail@example.com',
        'full_name': 'Иван Васильевич Doe',
        'id': 2,
        'nickname': 'ivandoe',
        'old_email': 'mail@example.com',
        'old_nickname': 'ivandoe',
        'roles': [],
    }

    def setUp(self):
        self.url = reverse('users:webhook-user-modified')
        util.mock_blender_id_responses()

        # Prepare a user
        self.user = OAuthUserFactory(
            email='mail@example.com',
            username='very-original-username',
            oauth_info__oauth_user_id=2,
            oauth_tokens__oauth_user_id=2,
            oauth_tokens__access_token='testaccesstoken',
            oauth_tokens__refresh_token='testrefreshtoken',
        )

    @responses.activate
    def test_user_modified_does_not_allow_duplicate_email(self):
        # Same email as in the webhook payload for another user
        another_user = OAuthUserFactory(email='jane@example.com')
        body = {
            **self.webhook_payload,
            'email': 'jane@example.com',
        }

        with self.assertLogs('users.views.webhooks', level='ERROR') as logs:
            response = self.client.post(
                self.url, body, content_type='application/json', **prepare_hmac_header(body)
            )
            self.assertRegex(logs.output[0], 'Unable to update email for')

        self.assertEqual(response.status_code, 204)
        self.assertEqual(response.content, b'')
        # Email was not updated
        self.assertEqual(self.user.email, 'mail@example.com')
        self.assertEqual(another_user.email, 'jane@example.com')

    @responses.activate
    def test_user_modified_does_not_allow_duplicate_username(self):
        # Same email as in the webhook payload for another user
        another_user = OAuthUserFactory(email='somename@example.com', username='thejane')
        body = {
            **self.webhook_payload,
            'nickname': 'thejane',
        }

        with self.assertLogs('users.views.webhooks', level='ERROR') as logs:
            response = self.client.post(
                self.url, body, content_type='application/json', **prepare_hmac_header(body)
            )
            self.assertRegex(logs.output[0], 'Unable to update username for')

        self.assertEqual(response.status_code, 204)
        self.assertEqual(response.content, b'')
        # Username was not updated
        self.assertEqual(self.user.username, 'very-original-username')
        self.assertEqual(another_user.username, 'thejane')

    @responses.activate
    def test_user_modified_does_not_allow_duplicate_email_and_username(self):
        # Same email as in the webhook payload for another user
        another_user = OAuthUserFactory(email='jane@example.com', username='thejane')
        body = {
            **self.webhook_payload,
            'email': 'jane@example.com',
            'nickname': 'thejane',
        }

        with self.assertLogs('users.views.webhooks', level='ERROR') as logs:
            response = self.client.post(
                self.url, body, content_type='application/json', **prepare_hmac_header(body)
            )
            self.assertRegex(logs.output[0], 'Unable to update email for')
            self.assertRegex(logs.output[1], 'Unable to update username for')

        self.assertEqual(response.status_code, 204)
        self.assertEqual(response.content, b'')
        # Email was not updated
        self.assertEqual(self.user.email, 'mail@example.com')
        self.assertEqual(another_user.email, 'jane@example.com')
        # Username was not updated
        self.assertEqual(self.user.username, 'very-original-username')
        self.assertEqual(another_user.username, 'thejane')
