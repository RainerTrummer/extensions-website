from django.test import TestCase
from django.urls import reverse

from common.tests.factories.extensions import create_version, create_approved_version
from common.tests.factories.teams import TeamFactory
from common.tests.factories.users import UserFactory, create_moderator
from teams.models import TeamsUsers


def _create_extension():
    extension = create_version(
        metadata__blender_version_min='4.2.0',
        metadata__name='Test Add-on',
        metadata__support='https://example.com/issues/',
        metadata__version='1.3.4',
        metadata__website='https://example.com/',
    ).extension
    extension.description = '**Description in bold**'
    extension.save(update_fields={'description'})
    return extension


class _BaseTestCase(TestCase):
    fixtures = ['dev', 'licenses']

    def _check_detail_page(self, response, extension):
        pass

    def _check_ratings_page(self, response, extension):
        pass


class PublicViewsTest(_BaseTestCase):
    def test_home_page_can_view_anonymously(self):
        [create_approved_version() for _ in range(3)]

        url = reverse('extensions:home')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_search_page_can_view_anonymously(self):
        [create_approved_version() for _ in range(3)]

        url = reverse('extensions:search')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_home_page_view_html(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'extensions/home.html')

    def test_no_one_can_view_extension_page_when_not_listed_404(self):
        # not listed by default
        extension = create_version().extension

        moderator = create_moderator()
        staff = UserFactory(is_staff=True)
        superuser = UserFactory(is_superuser=True, is_staff=True)
        author = extension.authors.first()

        url = extension.get_absolute_url()
        response = self.client.get(url)

        for account, role in (
            (None, 'anonymous'),
            (moderator, 'moderator'),
            (staff, 'staff'),
            (superuser, 'superuser'),
            (author, 'author'),
        ):
            with self.subTest(account=account, role=role):
                self.client.logout()
                if account:
                    self.client.force_login(account)
                self.assertEqual(response.status_code, 404)

    def test_anyone_can_view_extension_page_when_listed(self):
        extension = create_approved_version().extension

        moderator = create_moderator()
        staff = UserFactory(is_staff=True)
        superuser = UserFactory(is_superuser=True, is_staff=True)
        author = extension.authors.first()

        url = extension.get_absolute_url()
        response = self.client.get(url)

        for account, role in (
            (None, 'anonymous'),
            (moderator, 'moderator'),
            (staff, 'staff'),
            (superuser, 'superuser'),
            (author, 'author'),
        ):
            with self.subTest(role=role):
                self.client.logout()
                if account:
                    self.client.force_login(account)
                self.assertEqual(response.status_code, 200)


class ExtensionDetailViewTest(_BaseTestCase):
    def test_cannot_view_unlisted_extension_anonymously(self):
        extension = _create_extension()

        url = extension.get_absolute_url()
        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)

    def test_can_view_unlisted_extension_if_staff(self):
        staff_user = UserFactory(is_staff=True)
        extension = _create_extension()

        self.client.force_login(staff_user)
        response = self.client.get(extension.get_absolute_url())

        self._check_detail_page(response, extension)

    def test_can_view_unlisted_extension_if_maintainer(self):
        extension = _create_extension()

        self.client.force_login(extension.authors.first())
        response = self.client.get(extension.get_absolute_url())

        self._check_detail_page(response, extension)

    def test_can_view_unlisted_extension_if_team_member(self):
        extension = _create_extension()

        team = TeamFactory(slug='test-team')
        user = UserFactory()
        TeamsUsers(team=team, user=user).save()
        extension.team = team
        extension.save()

        self.client.force_login(user)
        response = self.client.get(extension.get_manage_url())

        self._check_detail_page(response, extension)

    def test_can_view_publicly_listed_extension_anonymously(self):
        extension = _create_extension()
        extension.approve()

        response = self.client.get(extension.get_absolute_url())

        self._check_detail_page(response, extension)

    def test_can_view_publicly_listed_extension_versions_anonymously(self):
        extension = _create_extension()
        extension.approve()

        response = self.client.get(extension.get_versions_url())

        self._check_detail_page(response, extension)

    def test_can_view_publicly_listed_extension_ratings_anonymously(self):
        extension = _create_extension()
        extension.approve()

        response = self.client.get(extension.get_ratings_url())

        self._check_ratings_page(response, extension)


class ExtensionManageViewTest(_BaseTestCase):
    def _check_manage_page(self, response, extension):
        self.assertContains(response, 'Test Add-on')
        self.assertContains(response, '**Description in bold**', html=True)
        self.assertContains(response, 'https://example.com/issues/')
        self.assertContains(response, 'https://example.com/')
        self.assertContains(response, 'Community', html=True)

    def test_cannot_view_manage_extension_page_anonymously(self):
        extension = _create_extension()

        response = self.client.get(extension.get_manage_url())

        self.assertEqual(response.status_code, 302)

    def test_cannot_view_manage_extension_page_for_drafts(self):
        extension = _create_extension()

        response = self.client.get(extension.get_manage_url())

        self.assertEqual(response.status_code, 302)

    def test_can_view_manage_extension_page_if_maintainer(self):
        extension = _create_extension()
        extension.approve()

        self.client.force_login(extension.authors.first())
        response = self.client.get(extension.get_manage_url())

        self._check_manage_page(response, extension)

    def test_can_view_manage_extension_page_if_team_member(self):
        extension = _create_extension()
        extension.approve()
        team = TeamFactory(slug='test-team')
        user = UserFactory()
        TeamsUsers(team=team, user=user).save()
        extension.team = team
        extension.save()

        self.client.force_login(user)
        response = self.client.get(extension.get_manage_url())

        self._check_manage_page(response, extension)


class UpdateVersionViewTest(_BaseTestCase):
    def test_update_view_access(self):
        extension = _create_extension()
        extension_owner = extension.latest_version.file.user
        extension.authors.add(extension_owner)

        random_user = UserFactory()

        url = reverse(
            'extensions:version-update',
            kwargs={
                'type_slug': extension.type_slug,
                'slug': extension.slug,
                'pk': extension.latest_version.pk,
            },
        )

        # Anonymous users are redirected to login
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        # Maintainers can view
        self.client.force_login(extension_owner)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # Others are forbidden
        self.client.force_login(random_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_blender_max_version(self):
        extension = _create_extension()
        extension_owner = extension.latest_version.file.user
        extension.authors.add(extension_owner)
        self.client.force_login(extension_owner)
        url = reverse(
            'extensions:version-update',
            kwargs={
                'type_slug': extension.type_slug,
                'slug': extension.slug,
                'pk': extension.latest_version.pk,
            },
        )
        version = extension.latest_version

        response = self.client.post(
            url,
            {'release_notes': 'text', 'blender_version_max': 'invalid'},
        )
        # error page, no redirect
        self.assertEqual(response.status_code, 200)
        version.refresh_from_db()
        self.assertIsNone(version.blender_version_max)

        response2 = self.client.post(
            url,
            {'release_notes': 'text', 'blender_version_max': '4.2.0'},
        )
        # error page (version must be greater), no redirect
        self.assertEqual(response2.status_code, 200)
        version.refresh_from_db()
        self.assertIsNone(version.blender_version_max)

        response3 = self.client.post(
            url,
            {'release_notes': 'text', 'blender_version_max': '4.2.1'},
        )
        self.assertEqual(response3.status_code, 302)
        version.refresh_from_db()
        self.assertEqual(version.blender_version_max, '4.2.1')

        # erase blender_version_max
        response4 = self.client.post(
            url,
            {'release_notes': 'text', 'blender_version_max': ''},
        )
        self.assertEqual(response4.status_code, 302)
        version.refresh_from_db()
        self.assertEqual(version.blender_version_max, None)


class MyExtensionsTest(_BaseTestCase):
    def test_team_members_see_extensions_in_my_extensions(self):
        extension = _create_extension()
        team = TeamFactory(slug='test-team')
        user = UserFactory()
        TeamsUsers(team=team, user=user).save()
        extension.team = team
        extension.save()

        self.client.force_login(user)
        response = self.client.get(reverse('extensions:manage-list'))
        self.assertContains(response, extension.name)
