from django.test import TestCase
from django.urls import reverse_lazy
from django.core.exceptions import ValidationError

from common.tests.factories.extensions import create_approved_version
from common.tests.factories.users import UserFactory

from constants.licenses import LICENSE_GPL3
from constants.base import EXTENSION_TYPE_CHOICES, EXTENSION_TYPE_SLUGS_SINGULAR
from extensions.models import Extension, Version, VersionPermission
from files.models import File
from files.validators import ManifestValidator, TagsAddonsValidator, TagsThemesValidator

import tempfile
import shutil
import os
import zipfile
import toml

META_DATA = {
    "id": "an_id",
    "name": "A random add-on",
    "tagline": "An add-on",
    "version": "0.1.0",
    "type": "add-on",
    "license": [LICENSE_GPL3.slug],
    "blender_version_min": "4.2.0",
    "blender_version_max": "4.3.0",
    "schema_version": "1.0.0",
    "maintainer": "",
    "tags": [],
}


class CreateFileTest(TestCase):
    submit_url = reverse_lazy('extensions:submit')

    def setUp(self):
        super().setUp()

        self.user = UserFactory()
        self.temp_directory = tempfile.mkdtemp()

        file_data = {
            "name": "Blender Kitsu",
            "id": "blender_kitsu",
            "version": "0.1.5",
        }
        self.file = self._create_file_from_data("blender_kitsu_1.5.0.zip", file_data, self.user)

    def tearDown(self):
        super().tearDown()
        shutil.rmtree(self.temp_directory)

    def _create_valid_extension(self, extension_id):
        return create_approved_version(
            metadata__id=extension_id,
            metadata__name='Blender Kitsu',
            metadata__version='0.1.5-alpha+f52258de',
            status=File.STATUSES.APPROVED,
        )

    def _create_file_from_data(self, filename, file_data, user):
        output_path = os.path.join(self.temp_directory, filename)
        manifest_path = os.path.join(self.temp_directory, "blender_manifest.toml")
        combined_meta_data = META_DATA.copy()
        combined_meta_data.update(file_data)

        version = combined_meta_data.get("version", "0.1.0")
        extension_id = combined_meta_data.get("id", "foobar").strip()
        type_slug = combined_meta_data['type']
        init_path = None

        if type_slug == 'add-on':
            # Add the required __init__.py file
            init_path = os.path.join(self.temp_directory, '__init__.py')
            with open(init_path, 'w') as init_file:
                init_file.write('')

        with open(manifest_path, "w") as manifest_file:
            toml.dump(combined_meta_data, manifest_file)

        with zipfile.ZipFile(output_path, "w") as my_zip:
            arcname = f"{extension_id}-{version}/{os.path.basename(manifest_path)}"
            my_zip.write(manifest_path, arcname=arcname)
            if init_path:
                # Write the __init__.py file too
                arcname = f"{extension_id}-{version}/{os.path.basename(init_path)}"
                my_zip.write(init_path, arcname=arcname)

        os.remove(manifest_path)
        return output_path


class ValidateManifestTest(CreateFileTest):
    fixtures = ['licenses']

    def test_validation_manifest_extension_id_hyphen(self):
        self.assertEqual(Extension.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        file_data = {
            "id": "id-with-hyphens",
        }

        bad_file = self._create_file_from_data("theme.zip", file_data, self.user)
        with open(bad_file, 'rb') as fp:
            response = self.client.post(self.submit_url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200)
        error = response.context['form'].errors.get('source')[0]
        self.assertIn('id-with-hyphens', error)
        self.assertIn('No hyphens', error)

    def test_validation_manifest_extension_id_spaces(self):
        self.assertEqual(Extension.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        file_data = {
            "id": "id with spaces",
        }

        bad_file = self._create_file_from_data("theme.zip", file_data, self.user)
        with open(bad_file, 'rb') as fp:
            response = self.client.post(self.submit_url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200)
        error = response.context['form'].errors.get('source')[0]
        self.assertIn('"id with spaces"', error)

    def test_validation_manifest_extension_id_clash(self):
        """Test if we add two extensions with the same extension_id"""
        self.assertEqual(Extension.objects.count(), 0)
        self._create_valid_extension('blender_kitsu')
        self.assertEqual(Extension.objects.count(), 1)

        user = UserFactory()
        self.client.force_login(user)

        kitsu_1_5 = {
            "name": "Blender Kitsu",
            "id": "blender_kitsu",
            "version": "0.1.5",
        }

        extension_file = self._create_file_from_data("theme.zip", kitsu_1_5, self.user)
        with open(extension_file, 'rb') as fp:
            response = self.client.post(self.submit_url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200)
        error = response.context['form'].errors.get('source')[0]
        self.assertIn('blender_kitsu', error)
        self.assertIn('already being used', error)

    def test_validation_manifest_name_clash(self):
        """Test if we add two extensions with the same name"""
        self.assertEqual(Extension.objects.count(), 0)
        self._create_valid_extension('blender_kitsu')
        self.assertEqual(Extension.objects.count(), 1)

        user = UserFactory()
        self.client.force_login(user)

        kitsu_1_5 = {
            "name": "Blender Kitsu",
            "id": "blender_kitsu2",
            "version": "0.1.5",
        }

        extension_file = self._create_file_from_data("theme.zip", kitsu_1_5, self.user)
        with open(extension_file, 'rb') as fp:
            response = self.client.post(self.submit_url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200)
        error = response.context['form'].errors.get('source')[0]
        self.assertIn('Blender Kitsu', error)
        self.assertIn('already being used', error)

    def test_validation_manifest_extension_id_mismatch(self):
        """Test if we try to add a new version to an extension with a mismatched extension_id"""
        self.assertEqual(Extension.objects.count(), 0)
        version = self._create_valid_extension('blender_kitsu')
        self.assertEqual(Extension.objects.count(), 1)

        # The same author is to send a new version to thte same extension
        self.client.force_login(version.file.user)

        non_kitsu_1_6 = {
            "name": "Blender Kitsu with a different ID",
            "id": "non_kitsu",
            "version": "0.1.6",
        }

        extension_file = self._create_file_from_data("theme.zip", non_kitsu_1_6, self.user)
        with open(extension_file, 'rb') as fp:
            response = self.client.post(
                version.extension.get_new_version_url(), {'source': fp, 'agreed_with_terms': True}
            )

        self.assertEqual(response.status_code, 200)
        error = response.context['form'].errors.get('source')[0]
        self.assertIn('non_kitsu', error)
        self.assertIn('blender_kitsu', error)
        self.assertIn('doesn\'t match the expected one', error)

    def test_validation_manifest_extension_id_repeated_version(self):
        """Test if we try to add a version to an extension without changing the version number"""
        self.assertEqual(Extension.objects.count(), 0)
        version = self._create_valid_extension('blender_kitsu')
        self.assertEqual(Extension.objects.count(), 1)

        # The same author is to send a new version to thte same extension
        self.client.force_login(version.file.user)

        kitsu_version_clash = {
            "name": "Change the name for lols",
            "id": version.extension.extension_id,
            "version": version.version,
        }

        extension_file = self._create_file_from_data(
            "kitsu_clash.zip", kitsu_version_clash, self.user
        )
        with open(extension_file, 'rb') as fp:
            response = self.client.post(
                version.extension.get_new_version_url(), {'source': fp, 'agreed_with_terms': True}
            )

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {
                'source': [
                    f'The version {version.version} was already uploaded for this extension ({ version.extension.name})'
                ]
            },
        )

    def test_upload_new_version_used_by_another_extension(self):
        """Test if we try to add a version to an extension but another extension is using this version number"""
        self.assertEqual(Extension.objects.count(), 0)
        version = self._create_valid_extension('blender_kitsu')
        self.assertEqual(Extension.objects.count(), 1)

        # Create another unrelated extension with the same target version we will use.
        create_approved_version(
            metadata__id='another_extension',
            metadata__name='Another Extension',
            metadata__version='0.1.6',
            status=File.STATUSES.APPROVED,
        )

        # The same author is to send a new version to thte same extension
        self.client.force_login(version.file.user)

        updated_kitsu = {
            "name": version.extension.name,
            "id": version.extension.extension_id,
            "version": '0.1.6',
        }

        extension_file = self._create_file_from_data("updated_kitsu.zip", updated_kitsu, self.user)
        with open(extension_file, 'rb') as fp:
            response = self.client.post(
                version.extension.get_new_version_url(), {'source': fp, 'agreed_with_terms': True}
            )

        self.assertEqual(response.status_code, 302)

    def test_validation_manifest_unsafe_id_hyphen(self):
        """Make sure the error message for id_hyphen is sane

        This is not a particular important check, but it uses the same logic of other parts of the code.
        So I'm using this test as a way to make sure all this logic is sound
        """
        self.assertEqual(Extension.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        file_data = {
            "id": "id-with-hyphens",
        }

        bad_file = self._create_file_from_data("theme.zip", file_data, self.user)
        with open(bad_file, 'rb') as fp:
            response = self.client.post(self.submit_url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200)
        error = response.context['form'].errors.get('source')
        self.assertEqual(len(error), 1)
        self.assertIn('"id-with-hyphens"', error[0])

    def test_name_left_as_is(self):
        user = UserFactory()
        self.client.force_login(user)

        file_data = {
            # If we ever need to restrict content of Extension's name,
            # it should be done at the manifest validation step.
            "name": "Name. - With  Extra spaces and other characters Ж",
        }

        extension_file = self._create_file_from_data("theme.zip", file_data, self.user)
        with open(extension_file, 'rb') as fp:
            response = self.client.post(self.submit_url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302)
        file = File.objects.first()
        extension = file.version.first().extension
        self.assertEqual(extension.slug, 'an-id')
        self.assertEqual(extension.name, 'Name. - With  Extra spaces and other characters Ж')


class ValidateManifestFields(TestCase):
    fixtures = ['licenses', 'version_permissions']

    def setUp(self):
        self.mandatory_fields = {
            key: item.example for (key, item) in ManifestValidator.mandatory_fields.items()
        }
        self.optional_fields = {
            key: item.example for (key, item) in ManifestValidator.optional_fields.items()
        }

    def test_missing_fields(self):
        with self.assertRaises(ValidationError) as e:
            ManifestValidator({})
        self.assertEqual(
            e.exception.messages,
            [
                'The following values are missing from the manifest file: blender_version_min, id, license, maintainer, name, schema_version, tagline, type, version'
            ],
        )

    def test_type_examples(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        # It should run fine without any exceptions.
        ManifestValidator(data)

    def test_wrong_type_fields(self):
        data = {
            'blender_version_min': 1,
            'id': 1,
            'license': 'SPDX:GPL-3.0-or-later',
            'maintainer': 1,
            'name': 1,
            'schema_version': 1,
            'tagline': 1,
            'type': 1,
            'version': 1,
        }

        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        self.assertEqual(len(e.exception.messages), len(data.keys()))

        flatten_err_message = ''.join(e.exception.messages)
        for key in data.keys():
            self.assertIn(key, flatten_err_message)

    def test_wrong_optional_type_fields(self):
        testing_data = {
            'blender_version_max': 1,
            'website': 1,
            'copyright': 1,
            'permissions': 'network',
            'tags': 'Development',
            'website': 1,
        }
        complete_data = {
            **self.mandatory_fields,
            **testing_data,
        }

        with self.assertRaises(ValidationError) as e:
            ManifestValidator(complete_data)

        self.assertEqual(len(e.exception.messages), len(testing_data))

        flatten_err_message = ''.join(e.exception.messages)
        for key in testing_data.keys():
            self.assertIn(key, flatten_err_message)

    def test_blender_version_min(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['blender_version_min'] = '4.2.0'
        ManifestValidator(data)

        data['blender_version_min'] = '2.9'
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        self.assertEqual(1, len(e.exception.messages))

        self.assertIn('blender_version_min', e.exception.messages[0])
        self.assertIn('4.2.0', e.exception.messages[0])

        data['blender_version_min'] = '4.1.0'
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(
            e.exception.messages,
            [
                'Manifest value error: <code>blender_version_min</code> should be at least "4.2.0"',
            ],
        )

        data['blender_version_min'] = '4.2.0-beta'
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(
            e.exception.messages,
            [
                'Manifest value error: <code>blender_version_min</code> should be at least "4.2.0"',
            ],
        )

    def test_blender_version_max(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['blender_version_min'] = '4.2.0'
        data['blender_version_max'] = '4.2.1'
        ManifestValidator(data)

        for bad_version in ['4.2.0', '4.1.99']:
            data['blender_version_max'] = bad_version
            with self.assertRaises(ValidationError) as e:
                ManifestValidator(data)
            self.assertEqual(1, len(e.exception.messages))
            self.assertIn('blender_version_max', e.exception.messages[0])

    def test_licenses(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['license'] = ['SPDX:GPL-2.0-or-later']
        ManifestValidator(data)

        data['license'] = ['SPDX:GPL-1.0']
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        message_begin = "Manifest value error: <code>license</code> expects a list of"
        self.assertIn(message_begin, e.exception.messages[0])
        self.assertIn('[\'SPDX:GPL-2.0-or-later\']', e.exception.messages[0])

        data['license'] = ['SPDX:GPL-2.0-only']
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(1, len(e.exception.messages))

    def test_platforms(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['platforms'] = ['linux-x64']
        ManifestValidator(data)

        data['platforms'] = ['linux-i386']
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        message_begin = "Manifest value error: <code>platforms</code> expects a list of"
        self.assertIn(message_begin, e.exception.messages[0])
        self.assertIn('linux-i386', e.exception.messages[0])

    def test_build_generated_platforms(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['build'] = {'generated': {'platforms': ['linux-x64']}}
        ManifestValidator(data)

        data['build'] = {'generated': {'platforms': ['linux-i386']}}
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        message_begin = "Manifest value error: <code>platforms</code> expects a list of"
        self.assertIn(message_begin, e.exception.messages[0])
        self.assertIn('linux-i386', e.exception.messages[0])

    def test_tags_addons(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }
        data['type'] = 'add-on'

        data['tags'] = ['Render']
        ManifestValidator(data)

        data['tags'] = ['UnsupportedTag']
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        message_begin = "Manifest value error: <code>tags</code> expects a list of"
        self.assertIn(message_begin, e.exception.messages[0])
        self.assertIn('[\'Animation\', \'Sequencer\']', e.exception.messages[0])

        data['tags'] = ['Dark']
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(1, len(e.exception.messages))

    def test_tags_themes(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }
        data.pop('permissions')
        data['type'] = 'theme'

        data['tags'] = ['Light']
        ManifestValidator(data)

        data['tags'] = ['UnsupportedTag']
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        message_begin = "Manifest value error: <code>tags</code> expects a list of"
        self.assertIn(message_begin, e.exception.messages[0])
        self.assertIn('[\'Dark\', \'Accessibility\']', e.exception.messages[0])

        data['tags'] = ['Render']
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(1, len(e.exception.messages))

    def test_permissions(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['permissions'] = {'files': 'test files'}
        ManifestValidator(data)

        data.pop('permissions')
        ManifestValidator(data)

        # punctuation
        data['permissions'] = {'files': 'lalala.'}
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        # empty reason
        data['permissions'] = {'files': ''}
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        # too long reason
        data['permissions'] = {
            'files': 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        }
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)

        data['permissions'] = {'non-supported-permission': 'lalala'}
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        message_begin = "Manifest value error: <code>permissions</code> expects key/value pairs of"
        self.assertIn(message_begin, e.exception.messages[0])
        self.assertIn('files, network', e.exception.messages[0])

        # Check the permissions that will always be around.
        message_end = e.exception.messages[0][len(message_begin) :]
        self.assertIn('files', message_end)
        self.assertIn('network', message_end)

        # Check for any other permission that we may add down the line.
        for permission in VersionPermission.objects.all():
            self.assertIn(permission.slug, message_end)

        data['permissions'] = {}
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(1, len(e.exception.messages))

        # Make sure permissions are only defined for add-ons, but fail for themes.
        data['permissions'] = {'files': 'test files'}
        data['type'] = EXTENSION_TYPE_SLUGS_SINGULAR[EXTENSION_TYPE_CHOICES.THEME]
        data['tags'] = []
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(1, len(e.exception.messages))

    def test_tagline(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['tagline'] = "Short tag-line"
        ManifestValidator(data)

        data['tagline'] = ''
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(1, len(e.exception.messages))
        self.assertIn('empty', e.exception.messages[0])

        for punctuation in ('.', '!', '?'):
            data['tagline'] = f'Tagline ending with a punctuation{punctuation}'
            with self.assertRaises(ValidationError) as e:
                ManifestValidator(data)
            self.assertEqual(1, len(e.exception.messages))
            self.assertIn('punctuation', e.exception.messages[0])

        data[
            'tagline'
        ] = 'Tagline that is suuper suuuuuper suuuuuuuuuuuuuuuuuuuuper suuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuper long'
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(1, len(e.exception.messages))
        self.assertIn('too long', e.exception.messages[0])

    def test_type(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }
        data.pop('permissions')

        # Good cops.
        data['type'] = EXTENSION_TYPE_SLUGS_SINGULAR[EXTENSION_TYPE_CHOICES.BPY]
        data['tags'] = TagsAddonsValidator.example
        ManifestValidator(data)

        data['type'] = EXTENSION_TYPE_SLUGS_SINGULAR[EXTENSION_TYPE_CHOICES.THEME]
        data['tags'] = TagsThemesValidator.example
        ManifestValidator(data)

        # Bad cops.
        # keymap and asset bundle are not part of the initial release of the extensions site.
        for type in ('keymap', 'asset-bundle', 'another-random-unsupported-type'):
            data['type'] = type
            with self.assertRaises(ValidationError) as e:
                ManifestValidator(data)
                self.assertEqual(1, len(e.exception.messages))

        data['type'] = ""
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
            self.assertEqual(1, len(e.exception.messages))

    def test_schema_version(self):
        data = {
            **self.mandatory_fields,
            **self.optional_fields,
        }

        data['schema_version'] = "1.0.0"
        ManifestValidator(data)

        data['schema_version'] = "0.0.9"
        with self.assertRaises(ValidationError) as e:
            ManifestValidator(data)
        self.assertEqual(1, len(e.exception.messages))
        self.assertIn('0.0.9', e.exception.messages[0])
        self.assertIn('not supported', e.exception.messages[0])


class VersionPermissionsTest(CreateFileTest):
    fixtures = ['licenses', 'version_permissions']

    def _test_version_permission(self, permissions):
        self.assertEqual(Extension.objects.count(), 0)
        version_original = self._create_valid_extension('blender_kitsu')
        extension = version_original.extension
        self.assertEqual(Extension.objects.count(), 1)
        self.assertEqual(Version.objects.count(), 1)
        self.assertEqual(version_original, extension.latest_version)

        # The same author is to send a new version to the same extension but with a new permission
        self.client.force_login(version_original.file.user)

        new_kitsu = {
            "id": "blender_kitsu",
            "version": "0.1.6",
            "permissions": permissions,
        }

        # Step 1: submit the file
        extension_file = self._create_file_from_data("kitsu-0.1.6.zip", new_kitsu, self.user)
        with open(extension_file, 'rb') as fp:
            response = self.client.post(
                extension.get_new_version_url(), {'source': fp, 'agreed_with_terms': True}
            )
        self.assertEqual(response.status_code, 302)

        # Check step 2: finalise new version and send to review
        url = response['Location']
        response = self.client.post(
            url,
            {
                'license': LICENSE_GPL3.id,
                'version': '2.0.1',
                'blender_version_min': '2.93.0',
                'blender_version_max': '3.2.0',
            },
        )
        self.assertEqual(response.status_code, 302)

        # The version needs to be approved.
        _file = File.objects.filter(original_name='kitsu-0.1.6.zip').first()
        _file.status = File.STATUSES.APPROVED
        _file.save()

        extension.refresh_from_db()
        self.assertNotEqual(version_original, extension.latest_version)
        self.assertEqual(Extension.objects.count(), 1)
        self.assertEqual(Version.objects.count(), 2)

        return extension.latest_version

    def test_version_permission_register(self):
        """Make sure permissions are saved"""
        permissions = {'network': 'talk to server'}
        latest_version = self._test_version_permission(permissions)
        self.assertEqual(latest_version.permissions.count(), 1)
        self.assertEqual(latest_version.permissions.first().slug, 'network')
