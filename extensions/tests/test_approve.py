from django.test import TestCase

from common.tests.factories.extensions import create_version
from files.models import File


class ApproveExtensionTest(TestCase):
    fixtures = ['licenses']

    def test_approve_extension(self):
        first_version = create_version()
        extension = first_version.extension
        self.assertFalse(extension.is_listed)
        extension.approve()
        self.assertTrue(extension.is_listed)

        # auto approve of new versions
        new_version = create_version(extension=extension)
        extension.refresh_from_db()
        self.assertEqual(new_version, extension.latest_version)
        self.assertTrue(new_version.is_listed)
        self.assertTrue(extension.is_listed)

        # TODO stop supporting direct file status updates, introduce methods for Version object
        # that would replace the signals logic

        # latest_version of approved extension must be listed
        # check that we rollback latest_version when file is not approved
        file = new_version.file
        file.status = File.STATUSES.AWAITING_REVIEW
        file.save(update_fields={'status'})
        new_version.refresh_from_db()
        self.assertFalse(new_version.is_listed)
        extension.refresh_from_db()
        self.assertEqual(first_version, extension.latest_version)
        self.assertTrue(extension.is_listed)

        # break the first_version, check that nothing is listed anymore
        first_version.file.status = File.STATUSES.AWAITING_REVIEW
        first_version.file.save()
        self.assertFalse(first_version.is_listed)
        extension.refresh_from_db()
        self.assertFalse(extension.is_listed)

        # this looks weird, but that's the current definition of latest_version, it's different
        # for listed and unlisted extensions:
        # now the extension is not listed, its latest_version doesn't have to be the latest
        # listed version
        self.assertEqual(new_version, extension.latest_version)
