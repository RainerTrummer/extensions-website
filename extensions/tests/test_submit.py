from pathlib import Path
from typing import List

from django.test import TestCase
from django.urls import reverse_lazy

from common.tests.factories.extensions import create_version
from common.tests.factories.files import FileFactory
from common.tests.factories.users import UserFactory
from common.tests.utils import _get_all_form_errors, CheckFilePropertiesMixin
from extensions.models import Extension, Version
from files.models import File
from reviewers.models import ApprovalActivity
import utils


TEST_FILES_DIR = Path(__file__).resolve().parent / 'files'
EXPECTED_EXTENSION_DATA = {
    'edit_breakdown-0.1.0.zip': {
        'metadata': {
            'tagline': 'Get insight on the complexity of an edit',
            'id': 'edit_breakdown',
            'name': 'Edit Breakdown',
            'version': '0.1.0',
            'blender_version_min': '4.2.0',
            'blender_version_max': '4.3.0',
            'type': 'add-on',
            'schema_version': "1.0.0",
        },
        'file_hash': 'sha256:28313858b9be34e6ecd15a63e28f626fb914dbdcc74c6d21c6536c9fad9de426',
        'size_bytes': 53969,
        'tags': ['Sequencer'],
        'version_str': '0.1.0',
        'slug': 'edit-breakdown',
    },
    'blender_gis-2.2.8.zip': {
        'metadata': {
            'tagline': 'Various tools for handle geodata',
            'name': 'BlenderGIS',
            'id': 'blender_gis',
            'version': '2.2.8',
            'blender_version_min': '4.2.0',
            'type': 'add-on',
        },
        'file_hash': 'sha256:fb71280e43400b1fd343a6b5a1421dcb63c4fa69935963429bd6cae965dad2db',
        'size_bytes': 434471,
        'tags': ['3D View'],
        'version_str': '2.2.8',
        'slug': 'blender-gis',
    },
    'amaranth-1.0.8.zip': {
        'metadata': {
            'tagline': 'A collection of tools and settings to improve productivity',
            'name': 'Amaranth',
            'id': 'amaranth',
            'version': '1.0.8',
            'blender_version_min': '4.2.0',
            'type': 'add-on',
        },
        'file_hash': 'sha256:09dcc1f0f9bc7103c48974da1d81f85b13326172fa008b2651cc4e77198654ed',
        'size_bytes': 72865,
        'tags': [],
        'version_str': '1.0.8',
        'slug': 'amaranth',
    },
    'addon-with-permissions.zip': {
        'metadata': {
            'tagline': 'Some add-on tag line',
            'name': 'Some Add-on',
            'id': 'some_addon',
            'version': '0.1.0',
            'blender_version_min': '4.2.0',
            'type': 'add-on',
            'permissions': {'files': 'reading files', 'network': 'talking to server'},
            'platforms': ['linux-x64'],
        },
        'file_hash': 'sha256:0431dac17d6e4d20c17a799dcc5ee915c12ea6d5b9a58a26b7850dea4aecc58c',
        'size_bytes': 765,
        'tags': [],
        'version_str': '0.1.0',
        'slug': 'some-addon',
    },
    'addon-with-split-platforms.zip': {
        'metadata': {
            'tagline': 'Some add-on tag line',
            'name': 'Some Add-on',
            'id': 'some_addon',
            'version': '0.1.0',
            'blender_version_min': '4.2.0',
            'type': 'add-on',
            'permissions': {'files': 'reading files', 'network': 'talking to server'},
            'platforms': ['linux-x64', 'windows-x64'],
            'build': {'generated': {'platforms': ['linux-x64']}},
        },
        'file_hash': 'sha256:b592384240eb04fb0f5e57741d93d8e15456d3fc27c837e2246422d6512ae002',
        'size_bytes': 791,
        'tags': [],
        'version_str': '0.1.0',
        'slug': 'some-addon',
    },
}
EXPECTED_VALIDATION_ERRORS = {
    'empty.txt': {'source': ['Only .zip files are accepted.']},
    'empty.zip': {'source': ['The submitted file is empty.']},
    'invalid-archive.zip': {'source': ['Only .zip files are accepted.']},
    'invalid-manifest-path.zip': {
        'source': [
            'The manifest file should be at the top level of the archive, or one level deep.',
        ],
    },
    'invalid-addon-no-init.zip': {
        'source': ['An add-on should have an __init__.py file.'],
    },
    'invalid-addon-dir-no-init.zip': {
        'source': ['An add-on should have an __init__.py file.'],
    },
    'invalid-no-manifest.zip': {
        'source': ['The manifest file is missing.'],
    },
    'invalid-manifest-toml.zip': {'source': ['Could not parse the manifest file.']},
    'invalid-theme-multiple-xmls.zip': {'source': ['A theme should have exactly one XML file.']},
    'invalid-missing-wheels.zip': {
        'source': [
            'A declared wheel is missing in the zip file, expected path: addon/wheels/test-wheel-whatever.whl'
        ]
    },
}
POST_DATA = {
    'preview_set-TOTAL_FORMS': ['0'],
    'preview_set-INITIAL_FORMS': ['0'],
    'preview_set-MIN_NUM_FORMS': ['0'],
    'preview_set-MAX_NUM_FORMS': ['1000'],
    'preview_set-0-id': [''],
    # 'preview_set-0-extension': [str(extension.pk)],
    'preview_set-1-id': [''],
    # 'preview_set-1-extension': [str(extension.pk)],
    'form-TOTAL_FORMS': ['0'],
    'form-INITIAL_FORMS': ['0'],
    'form-MIN_NUM_FORMS': ['0'],
    'form-MAX_NUM_FORMS': ['1000'],
    'form-0-id': '',
    # 'form-0-caption': ['First Preview Caption Text'],
    'form-1-id': '',
    # 'form-1-caption': ['Second Preview Caption Text'],
}


class SubmitFileTest(TestCase):
    maxDiff = None
    fixtures = ['licenses']
    url = reverse_lazy('extensions:submit')

    def _test_submit_addon(
        self,
        file_name: str,
        name: str,
        tags: List[str],
        version_str: str,
        blender_version_min: str,
        size_bytes: int,
        file_hash: str,
        slug: str,
        **other_metadata,
    ):
        self.assertEqual(File.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        with open(TEST_FILES_DIR / file_name, 'rb') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(File.objects.count(), 1)
        file = File.objects.first()
        self.assertEqual(response['Location'], file.version.first().extension.get_draft_url())
        extension = file.version.first().extension
        self.assertEqual(extension.slug, slug)
        self.assertEqual(extension.name, name)
        self.assertEqual(file.original_name, file_name)
        self.assertEqual(file.size_bytes, size_bytes)
        self.assertEqual(file.original_hash, file_hash)
        self.assertEqual(file.hash, file_hash)
        self.assertEqual(file.get_type_display(), 'Add-on')
        self.assertEqual(file.metadata['version'], version_str)
        self.assertEqual(file.metadata.get('permissions'), other_metadata.get('permissions'))
        self.assertEqual(
            [p.slug for p in file.version.first().platforms.all()],
            other_metadata.get('build', {}).get('generated', {}).get('platforms', [])
            or other_metadata.get('platforms', []),
        )

    def test_not_allowed_anonymous(self):
        with open(TEST_FILES_DIR / 'edit_breakdown-0.1.0.zip', 'rb') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/oauth/login?next={self.url}')

    def test_validation_errors_agreed_with_terms_required(self):
        self.assertEqual(Extension.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        with open(TEST_FILES_DIR / 'theme.zip', 'rb') as fp:
            response = self.client.post(self.url, {'source': fp})

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {'agreed_with_terms': ['This field is required.']},
        )

    def test_validation_errors(self):
        self.assertEqual(Extension.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        for test_archive, expected_errors in EXPECTED_VALIDATION_ERRORS.items():
            with self.subTest(test_archive=test_archive):
                with open(TEST_FILES_DIR / test_archive, 'rb') as fp:
                    response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

                self.assertEqual(response.status_code, 200)
                self.assertDictEqual(response.context['form'].errors, expected_errors)

    def test_addon_without_top_level_directory(self):
        self.assertEqual(Extension.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        with open(TEST_FILES_DIR / 'addon-without-dir.zip', 'rb') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302)

    def test_theme_file(self):
        self.assertEqual(File.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        with open(TEST_FILES_DIR / 'theme.zip', 'rb') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302, _get_all_form_errors(response))
        self.assertEqual(File.objects.count(), 1)
        file = File.objects.first()
        self.assertEqual(response['Location'], file.version.first().extension.get_draft_url())
        self.assertEqual(file.user, user)
        self.assertEqual(file.original_name, 'theme.zip')
        self.assertEqual(file.size_bytes, 5895)
        self.assertEqual(
            file.original_hash,
            'sha256:e097f7a7e34e2d9d7a1ad9bffc5556e97b9ea668144580695ff42ede1c6027a7',
        )
        self.assertEqual(
            file.hash, 'sha256:e097f7a7e34e2d9d7a1ad9bffc5556e97b9ea668144580695ff42ede1c6027a7'
        )
        self.assertEqual(file.get_type_display(), 'Theme')
        self.assertEqual(file.metadata['name'], 'Theme')

    def test_keymap_file(self):
        self.assertEqual(File.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        with open(TEST_FILES_DIR / 'keymap.zip', 'rb') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200, _get_all_form_errors(response))
        self.assertEqual(File.objects.count(), 0)

    def test_asset_bundle_file(self):
        self.assertEqual(File.objects.count(), 0)
        user = UserFactory()
        self.client.force_login(user)

        with open(TEST_FILES_DIR / 'asset_bundle-0.1.0.zip', 'rb') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200, _get_all_form_errors(response))
        self.assertEqual(File.objects.count(), 0)


for file_name, data in EXPECTED_EXTENSION_DATA.items():

    def _create_test_method(file_name, data):
        def _method(self):
            kwargs = data.copy()
            metadata = kwargs.pop('metadata')
            self._test_submit_addon(file_name, **kwargs, **metadata)

        return _method

    setattr(
        SubmitFileTest,
        f'test_submit_addon_file_{utils.slugify(file_name).replace("-", "_")}',
        _create_test_method(file_name, data),
    )


class SubmitFinaliseTest(CheckFilePropertiesMixin, TestCase):
    maxDiff = None
    fixtures = ['licenses']

    def setUp(self):
        super().setUp()
        file_data = EXPECTED_EXTENSION_DATA['edit_breakdown-0.1.0.zip']
        user = UserFactory()
        self.file = FileFactory(
            type=File.TYPES.BPY,
            user=user,
            original_hash=file_data['file_hash'],
            hash=file_data['file_hash'],
            metadata=file_data['metadata'],
        )
        self.version = create_version(file=self.file)

    def test_get_finalise_addon_redirects_if_anonymous(self):
        response = self.client.post(self.file.version.first().extension.get_draft_url(), {})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response['Location'],
            f'/oauth/login?next=/add-ons/{self.file.version.first().extension.slug}/draft/',
        )

    def test_get_finalise_addon_not_allowed_if_different_user(self):
        user = UserFactory()
        self.client.force_login(user)

        response = self.client.post(self.file.version.first().extension.get_draft_url(), {})

        # Technically this could (should) be a 403, but changing this means changing
        # the MaintainedExtensionMixin which is used in multiple places.
        self.assertEqual(response.status_code, 404)

    def test_post_finalise_addon_validation_errors(self):
        self.client.force_login(self.file.user)
        data = {**POST_DATA, 'submit_draft': ''}
        response = self.client.post(self.file.version.first().extension.get_draft_url(), data)

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            _get_all_form_errors(response),
            {
                'form': [{}, None],
                'extension_form': [{'description': ['This field is required.']}, None],
                'add_preview_formset': [[], ['Please add at least one preview.']],
                'edit_preview_formset': [[], []],
                'featured_image_form': [{'source': ['This field is required.']}, None],
                'icon_form': [{'source': ['This field is required.']}, None],
                'image_form': [{'source': ['This field is required.']}, None],
            },
        )

    def test_post_finalise_addon_creates_addon_with_version_awaiting_review(self):
        self.assertEqual(File.objects.count(), 1)
        self.assertEqual(Extension.objects.count(), 1)
        self.assertEqual(Version.objects.count(), 1)
        images_count_before = File.objects.filter(type=File.TYPES.IMAGE).count()

        self.client.force_login(self.file.user)
        data = {
            # Most of these values should come from the form's initial values, set in the template
            # Version fields
            'release_notes': 'initial release',
            # Extension fields
            'description': 'Rather long and verbose description',
            'support': 'https://example.com/issues',
            # Previews
            'form-TOTAL_FORMS': ['2'],
            'form-INITIAL_FORMS': ['0'],
            'form-MIN_NUM_FORMS': ['0'],
            'form-MAX_NUM_FORMS': ['1000'],
            'form-0-id': '',
            'form-0-caption': ['First Preview Caption Text'],
            'form-1-id': '',
            'form-1-caption': ['Second Preview Caption Text'],
            # Edit form for existing previews
            'preview_set-TOTAL_FORMS': ['0'],
            'preview_set-INITIAL_FORMS': ['0'],
            'preview_set-MIN_NUM_FORMS': ['0'],
            'preview_set-MAX_NUM_FORMS': ['1000'],
            'preview_set-0-id': [''],
            # 'preview_set-0-extension': [str(extension.pk)],
            'preview_set-1-id': [''],
            # 'preview_set-1-extension': [str(extension.pk)],
            # Submit for Approval.
            'submit_draft': '',
        }
        file_name1 = 'test_preview_image_0001.png'
        file_name2 = 'test_preview_image_0002.png'
        file_name3 = 'test_icon_0001.png'
        file_name4 = 'test_featured_image_0001.png'
        with open(TEST_FILES_DIR / file_name1, 'rb') as fp1, open(
            TEST_FILES_DIR / file_name2, 'rb'
        ) as fp2, open(TEST_FILES_DIR / file_name3, 'rb') as fp3, open(
            TEST_FILES_DIR / file_name4, 'rb'
        ) as fp4:
            files = {
                'form-0-source': fp1,
                'form-1-source': fp2,
                'icon-source': fp3,
                'featured-image-source': fp4,
            }
            response = self.client.post(
                self.file.version.first().extension.get_draft_url(), {**data, **files}
            )

        self.assertEqual(response.status_code, 302, _get_all_form_errors(response))
        self.assertEqual(response['Location'], '/add-ons/edit-breakdown/manage/')
        self.assertEqual(File.objects.filter(type=File.TYPES.BPY).count(), 1)
        self.assertEqual(Extension.objects.count(), 1)
        self.assertEqual(Version.objects.count(), 1)
        # Check that 4 new images had been created
        self.assertEqual(
            File.objects.filter(type=File.TYPES.IMAGE).count(), images_count_before + 4
        )
        # Check an add-on was created with all given fields
        extension = Extension.objects.first()
        self.assertEqual(extension.get_type_display(), 'Add-on')
        self.assertEqual(extension.get_status_display(), 'Awaiting Review')
        self.assertEqual(extension.name, 'Edit Breakdown')
        self.assertEqual(extension.website, None)
        self.assertEqual(extension.support, data['support'])
        self.assertEqual(extension.description, data['description'])
        self.assertEqual(list(extension.authors.all()), [self.file.user])
        # Check an add-on version was created with all given fields
        version = extension.latest_version
        self.assertEqual(version.version, '0.1.0')
        self.assertEqual(version.blender_version_min, '4.2.0')
        self.assertEqual(version.blender_version_max, None)
        self.assertEqual(version.schema_version, '1.0.0')
        self.assertEqual(version.release_notes, data['release_notes'])
        # Check version file properties
        self._test_file_properties(
            version.file,
            content_type='application/zip',
            get_status_display='Awaiting Review',
            get_type_display='Add-on',
            hash=version.file.original_hash,
            original_hash='sha256:2831385',
        )
        # We cannot check for the ManyToMany yet (tags, licences, permissions)

        # Check icon file properties
        self._test_file_properties(
            extension.icon,
            content_type='image/png',
            get_status_display='Awaiting Review',
            get_type_display='Image',
            hash=extension.icon.original_hash,
            name='images/ee/ee3a015',
            original_hash='sha256:ee3a015',
            original_name='test_icon_0001.png',
            size_bytes=30177,
        )

        # Check featured image file properties
        self._test_file_properties(
            extension.featured_image,
            content_type='image/png',
            get_status_display='Awaiting Review',
            get_type_display='Image',
            hash=extension.featured_image.original_hash,
            name='images/a3/a3f445bfadc6a',
            original_hash='sha256:a3f445bfadc6a',
            original_name='test_featured_image_0001.png',
            size_bytes=155684,
        )

        # Check properties of preview image files
        self._test_file_properties(
            extension.previews.all()[0],
            content_type='image/png',
            get_status_display='Awaiting Review',
            get_type_display='Image',
            hash='sha256:643e15',
            name='images/64/643e15',
            original_hash='sha256:643e15',
            original_name='test_preview_image_0001.png',
            size_bytes=1163,
        )
        self._test_file_properties(
            extension.previews.all()[1],
            content_type='image/png',
            get_status_display='Awaiting Review',
            get_type_display='Image',
            hash='sha256:f8ef448d',
            name='images/f8/f8ef448d',
            original_hash='sha256:f8ef448d',
            original_name='test_preview_image_0002.png',
            size_bytes=1693,
        )

        # Check that author can access the page they are redirected to
        response = self.client.get(response['Location'])
        self.assertEqual(response.status_code, 200)


class NewVersionTest(TestCase):
    fixtures = ['licenses']

    def setUp(self):
        self.version = create_version(metadata__id='amaranth')
        self.extension = self.version.extension
        self.url = self.extension.get_new_version_url()

    def test_not_allowed_anonymous(self):
        with open(TEST_FILES_DIR / 'amaranth-1.0.8.zip', 'rb') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302)
        self.assertTrue(response['Location'].startswith('/oauth/login'))

    def test_validation_errors_agreed_with_terms_required(self):
        self.client.force_login(self.version.file.user)

        with open(TEST_FILES_DIR / 'amaranth-1.0.8.zip', 'rb') as fp:
            response = self.client.post(self.url, {'source': fp})

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {'agreed_with_terms': ['This field is required.']},
        )

    def test_validation_errors_invalid_extension(self):
        self.client.force_login(self.version.file.user)

        with open(TEST_FILES_DIR / 'empty.txt', 'rb') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {'source': ['Only .zip files are accepted.']},
        )

    def test_validation_errors_empty_file(self):
        self.client.force_login(self.version.file.user)

        with open(TEST_FILES_DIR / 'empty.zip', 'rb') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {'source': ['The submitted file is empty.']},
        )

    def test_upload_new_file_and_finalise_new_version(self):
        self.client.force_login(self.version.file.user)
        self.extension.approve()

        # Check step 1: upload a new file
        with open(TEST_FILES_DIR / 'amaranth-1.0.8.zip', 'rb') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302)
        file = File.objects.order_by('-date_created').first()
        self.assertEqual(
            response['Location'],
            f'/add-ons/{self.extension.slug}/manage/versions/new/{file.pk}/',
        )
        # now a file upload creates a corresponding version object immediately
        self.assertEqual(self.extension.versions.count(), 2)
        new_version = self.extension.versions.order_by('date_created').last()
        self.assertEqual(new_version.version, '1.0.8')
        self.assertEqual(new_version.blender_version_min, '4.2.0')
        self.assertEqual(new_version.schema_version, '1.0.0')
        self.assertEqual(new_version.file.get_status_display(), 'Approved')
        self.assertEqual(new_version.release_notes, '')
        self.assertEqual(
            ApprovalActivity.objects.filter(
                extension=self.extension,
                type=ApprovalActivity.ActivityType.UPLOADED_NEW_VERSION,
            ).count(),
            1,
        )

        url = response['Location']
        response = self.client.post(
            url,
            {
                'release_notes': 'new version',
            },
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/add-ons/{self.extension.slug}/manage/versions/')
        self.assertEqual(self.extension.versions.count(), 2)
        new_version.refresh_from_db()
        self.assertEqual(new_version.release_notes, 'new version')


class DraftsWarningTest(TestCase):
    fixtures = ['licenses']

    def test_page_contains_warning(self):
        version = create_version()
        extension = version.extension
        self.assertEqual(extension.status, Extension.STATUSES.DRAFT)
        self.client.force_login(extension.authors.all()[0])
        response = self.client.get(reverse_lazy('extensions:submit'))
        self.assertContains(response, extension.get_draft_url())
