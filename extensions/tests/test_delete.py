from pathlib import Path
import json

from django.contrib.admin.models import LogEntry, DELETION
from django.test import TestCase, override_settings

from common.tests.factories.extensions import RatingFactory, create_approved_version, create_version
from common.tests.factories.files import FileFactory
from common.tests.factories.users import UserFactory, create_moderator
import extensions.models
import files.models
import reviewers.models

TEST_MEDIA_DIR = Path(__file__).resolve().parent / 'media'


# Media file are physically deleted when files records are deleted, hence the override
@override_settings(MEDIA_ROOT=TEST_MEDIA_DIR)
class DeleteTest(TestCase):
    fixtures = ['dev', 'licenses']

    def test_unlisted_unrated_extension_can_be_deleted_by_author(self):
        self.maxDiff = None
        reused_image = FileFactory(
            type=files.models.File.TYPES.IMAGE,
            original_name='extension_feature_image.png',
            source='images/b0/b03fa981527593fbe15b28cf37c020220c3d83021999eab036b87f3bca9c9168.png',
        )
        version = create_version(status=files.models.File.STATUSES.AWAITING_REVIEW)
        extension = version.extension
        extension.featured_image = reused_image
        extension.icon = FileFactory(
            type=files.models.File.TYPES.IMAGE,
            original_name='extension_icon_final.png',
            source='images/8a/8a01102de8573d50bbc90033f55f232b7cacc4f1eb3e3c3d851615841d2956e1.png',
        )
        extension.save(update_fields={'featured_image', 'icon'})
        extension.previews.add(reused_image)
        extension.previews.add(
            FileFactory(
                type=files.models.File.TYPES.IMAGE,
                original_name='extension_preview_001.png',
                source='images/b0/b03fa981527593fbe15b28cf37c020220c3d83021999eab036b87f3bca9c9168.png',
            )
        )
        version_file = version.file
        icon = extension.icon
        featured_image = extension.featured_image
        self.assertEqual(version_file.get_status_display(), 'Awaiting Review')
        self.assertEqual(extension.get_status_display(), 'Draft')
        self.assertFalse(extension.is_listed)
        self.assertEqual(extension.cannot_be_deleted_reasons, [])
        preview_file = extension.previews.last()
        self.assertIsNotNone(preview_file)
        # Create some ApprovalActivity as well
        moderator = create_moderator()
        approval_activity = reviewers.models.ApprovalActivity.objects.create(
            extension=extension,
            user=moderator,
            message='This is a message in approval activity',
        )
        # Create a file validation record
        file_validation = files.models.FileValidation.objects.create(
            file=version_file, results={'deadbeef': 'foobar'}
        )
        # FIXME? version is listed twice, because unfortunately we generate duplicate log entry:
        # 1. it happens due to Extension deleting its Version objects via CASCADE
        # 2. it happens in post_delete for VersionFile when the check for the last file is done
        object_reprs = list(
            map(
                repr,
                [
                    version,
                    version_file,
                    file_validation,
                    extension,
                    version,
                    approval_activity,
                    preview_file.preview_set.first(),
                    reused_image.preview_set.first(),
                ],
            )
        )

        url = extension.get_delete_url()
        user = extension.authors.first()
        self.client.force_login(user)
        response = self.client.post(url)

        self.assertEqual(response.status_code, 302)
        # All relevant records should have been deleted
        with self.assertRaises(extensions.models.Extension.DoesNotExist):
            extension.refresh_from_db()
        with self.assertRaises(extensions.models.Version.DoesNotExist):
            version.refresh_from_db()
        with self.assertRaises(files.models.File.DoesNotExist):
            version_file.refresh_from_db()
        # Preview files aren't deleted: they might be re-uploaded shortly and should be looked up by hash
        preview_file.refresh_from_db()
        icon.refresh_from_db()
        featured_image.refresh_from_db()
        self.assertIsNone(extensions.models.Extension.objects.filter(pk=extension.pk).first())
        self.assertIsNone(extensions.models.Version.objects.filter(pk=version.pk).first())
        self.assertIsNone(
            extensions.models.VersionFile.objects.filter(version__pk=version.pk).first()
        )
        self.assertIsNone(files.models.File.objects.filter(pk=version_file.pk).first())
        self.assertIsNotNone(files.models.File.objects.filter(pk=preview_file.pk).first())
        self.assertIsNotNone(files.models.File.objects.filter(pk=icon.pk).first())
        self.assertIsNotNone(files.models.File.objects.filter(pk=featured_image.pk).first())

        # Check that each of the deleted records was logged
        deletion_log_entries_q = LogEntry.objects.filter(action_flag=DELETION)
        self.assertEqual(
            [_.object_repr for _ in deletion_log_entries_q],
            object_reprs,
        )
        log_entry = deletion_log_entries_q.filter(object_repr__contains='Extension').first()
        change_message_data = json.loads(log_entry.change_message)
        self.assertEqual(
            change_message_data[0]['deleted']['object'], f'<Extension: Add-on "{extension.name}">'
        )
        self.assertEqual(
            set(change_message_data[0]['deleted']['old_state'].keys()),
            {
                'average_score',
                'date_approved',
                'date_created',
                'date_modified',
                'date_status_changed',
                'description',
                'download_count',
                'extension_id',
                'featured_image',
                'icon',
                'is_listed',
                'latest_version',
                'name',
                'pk',
                'rating_sortkey',
                'slug',
                'status',
                'support',
                'team',
                'type',
                'view_count',
                'website',
            },
        )
        self.assertEqual(
            log_entry.get_change_message(),
            f'Deleted extension “<Extension: Add-on "{extension.name}">”.',
        )

        # TODO: check that files were deleted from storage (create a temp one prior to the check)

    def test_publicly_listed_extension_cannot_be_deleted(self):
        version = create_approved_version()
        self.assertTrue(version.is_listed)
        extension = version.extension
        self.assertTrue(extension.is_listed)
        self.assertEqual(extension.get_status_display(), 'Approved')

        self.assertEqual(version.cannot_be_deleted_reasons, ['version_is_listed'])
        self.assertEqual(extension.cannot_be_deleted_reasons, ['is_listed', 'version_is_listed'])

        url = extension.get_delete_url()
        user = extension.authors.first()
        self.client.force_login(user)
        response = self.client.post(url)

        self.assertEqual(response.status_code, 403)

    def test_rated_extension_cannot_be_deleted(self):
        version = create_version(status=files.models.File.STATUSES.AWAITING_REVIEW)
        RatingFactory(version=version)
        self.assertFalse(version.is_listed)
        extension = version.extension
        self.assertFalse(extension.is_listed)
        self.assertEqual(extension.get_status_display(), 'Draft')

        self.assertEqual(version.cannot_be_deleted_reasons, ['version_has_ratings'])
        self.assertEqual(
            extension.cannot_be_deleted_reasons, ['has_ratings', 'version_has_ratings']
        )

        url = extension.get_delete_url()
        user = extension.authors.first()
        self.client.force_login(user)
        response = self.client.post(url)

        self.assertEqual(response.status_code, 403)

    def test_reported_extension_cannot_be_deleted(self):  # TODO
        pass

    def test_extension_with_ratings_cannot_be_deleted(self):
        version = create_approved_version()
        extension = version.extension
        self.assertEqual(extension.status, extension.STATUSES.APPROVED)

        url = extension.get_delete_url()
        user = extension.authors.first()
        self.client.force_login(user)
        response = self.client.post(url)

        self.assertEqual(response.status_code, 403)

    def test_random_user_cant_delete(self):
        extension = create_approved_version().extension

        url = extension.get_delete_url()
        user = UserFactory()
        self.client.force_login(user)
        response = self.client.post(url)

        self.assertEqual(response.status_code, 403)
        extension.refresh_from_db()
