from datetime import timedelta
from pathlib import Path

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from common.tests.factories.users import UserFactory
from common.tests.factories.extensions import create_approved_version, create_version
from common.tests.utils import create_user_token

from extensions.models import Extension, Version
from files.models import File
from teams.models import Team


TEST_FILES_DIR = Path(__file__).resolve().parent / 'files'


class ListedExtensionsTest(APITestCase):
    def setUp(self):
        self.assertEqual(Extension.objects.count(), 0)
        self.assertEqual(Version.objects.count(), 0)
        self.version = create_approved_version()
        self.extension = self.version.extension
        self.assertEqual(Extension.objects.count(), 1)
        self.assertEqual(Version.objects.count(), 1)
        self.assertTrue(self.extension.is_listed)
        self.assertEqual(self._listed_extensions_count(), 1)

    def _listed_extensions_count(self):
        response = self.client.get('/api/v1/extensions/', HTTP_ACCEPT='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

        # Basic sanity check to make sure we are getting the result of listed
        listed_count = len(response.json()['data'])
        self.assertEqual(Extension.objects.listed.count(), listed_count)
        return listed_count

    def test_list_extension_only_once(self):
        create_approved_version(extension=self.extension)
        self.assertEqual(self._listed_extensions_count(), 1)

    def test_moderate_extension(self):
        self.extension.status = Extension.STATUSES.DISABLED
        self.extension.save()
        self.assertEqual(self._listed_extensions_count(), 0)

    def test_moderate_only_version(self):
        self.version.file.status = File.STATUSES.DISABLED
        self.version.file.save()
        self.assertEqual(self._listed_extensions_count(), 0)


class ResponseFormatTest(APITestCase):
    def test_json_format(self):
        [create_approved_version() for _ in range(3)]
        response = self.client.get('/api/v1/extensions/', HTTP_ACCEPT='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')
        json = response.json()
        self.assertEqual(len(json['data']), 3)
        for v in json['data']:
            self.assertIn('id', v)
            self.assertIn('name', v)
            self.assertIn('tagline', v)
            self.assertIn('version', v)
            self.assertIn('type', v)
            self.assertIn('archive_size', v)
            self.assertIn('archive_hash', v)
            self.assertIn('archive_url', v)
            self.assertIn('blender_version_min', v)
            self.assertIn('maintainer', v)
            self.assertIn('license', v)
            self.assertIn('website', v)
            self.assertIn('schema_version', v)
            # Blender expects urls in HTML anchors to end with .zip to handle drag&drop
            self.assertEqual(v['archive_url'][-4:], '.zip')

    def test_maintaner_is_team(self):
        version = create_approved_version(metadata__blender_version_min='4.0.1')
        team = Team(name='test team', slug='test-team')
        team.save()
        version.extension.team = team
        version.extension.save()
        url = reverse('extensions:api')

        json = self.client.get(
            url,
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(json['data'][0]['maintainer'], 'test team')

    def test_blocklist(self):
        extension = create_version().extension
        extension.status = extension.STATUSES.BLOCKLISTED
        extension.save()
        url = reverse('extensions:api')

        json = self.client.get(
            url + '?blender_version=4.1.1',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(json['blocklist'], [extension.extension_id])


class FiltersTest(APITestCase):
    def test_blender_version_filter(self):
        create_approved_version(metadata__blender_version_min='4.0.1')
        create_approved_version(metadata__blender_version_min='4.1.1')
        create_approved_version(metadata__blender_version_min='4.2.1')
        url = reverse('extensions:api')

        json = self.client.get(
            url + '?blender_version=4.1.1',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(len(json['data']), 2)

        json2 = self.client.get(
            url + '?blender_version=3.0.1',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(len(json2['data']), 0)

        json3 = self.client.get(
            url + '?blender_version=4.3.1',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(len(json3['data']), 3)

    def test_platform_filter(self):
        create_approved_version(metadata__platforms=['windows-x64'])
        create_approved_version(metadata__platforms=['windows-arm64'])
        create_approved_version()
        url = reverse('extensions:api')

        # returns 1st and 3rd items
        json = self.client.get(
            url + '?platform=windows-x64',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(len(json['data']), 2)

        # only returns the 3rd item
        json = self.client.get(
            url + '?platform=platform-we-dont-know',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(len(json['data']), 1)

    def test_blender_version_filter_latest_not_max_version(self):
        version = create_approved_version(metadata__blender_version_min='4.0.1')
        date_created = version.date_created
        extension = version.extension
        version = create_approved_version(
            extension=extension,
            metadata__blender_version_min='4.2.1',
            metadata__version='2.0.0',
        )
        version.date_created = date_created + timedelta(days=1)
        version.save(update_fields={'date_created'})
        create_approved_version(
            extension=extension,
            metadata__blender_version_min='3.0.0',
            metadata__version='1.0.1',
        )
        version.date_created = date_created + timedelta(days=2)
        version.save(update_fields={'date_created'})
        create_approved_version(
            extension=extension,
            metadata__blender_version_min='4.2.1',
            metadata__version='2.0.1',
        )
        version.date_created = date_created + timedelta(days=3)
        version.save(update_fields={'date_created'})
        url = reverse('extensions:api')

        json = self.client.get(
            url + '?blender_version=4.1.1',
            HTTP_ACCEPT='application/json',
        ).json()
        self.assertEqual(len(json['data']), 1)
        # we are expecting the latest matching, not the maximum version
        self.assertEqual(json['data'][0]['version'], '1.0.1')


class VersionUploadAPITest(APITestCase):
    def setUp(self):
        self.user = UserFactory()
        self.token, self.token_key = create_user_token(user=self.user)

        self.client = APIClient()
        self.version = create_approved_version(
            metadata__id="amaranth",
            metadata__version="1.0.7",
            user=self.user,
        )
        self.extension = self.version.extension
        self.file_path = TEST_FILES_DIR / "amaranth-1.0.8.zip"

    @staticmethod
    def _get_upload_url(extension_id):
        upload_url = reverse('extensions:upload-extension-version', args=(extension_id,))
        return upload_url

    def test_version_upload_unauthenticated(self):
        with open(self.file_path, 'rb') as version_file:
            response = self.client.post(
                self._get_upload_url(self.extension.extension_id),
                {
                    'version_file': version_file,
                    'release_notes': 'These are the release notes',
                },
                format='multipart',
            )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_version_upload_extension_not_maintained_by_user(self):
        other_user = UserFactory()
        other_extension = create_approved_version(
            metadata__id='other_extension', user=other_user
        ).extension

        with open(self.file_path, 'rb') as version_file:
            response = self.client.post(
                self._get_upload_url(other_extension.extension_id),
                {
                    'version_file': version_file,
                    'release_notes': 'These are the release notes',
                },
                format='multipart',
                HTTP_AUTHORIZATION=f'Bearer {self.token_key}',
            )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.data['message'],
            f'Extension "{other_extension.extension_id}" not maintained by user "{self.user.username}"',
        )

    def test_version_upload_extension_does_not_exist(self):
        extension_name = 'extension_do_not_exist'
        with open(self.file_path, 'rb') as version_file:
            response = self.client.post(
                self._get_upload_url(extension_name),
                {
                    'version_file': version_file,
                    'release_notes': 'These are the release notes',
                },
                format='multipart',
                HTTP_AUTHORIZATION=f'Bearer {self.token_key}',
            )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data['message'], f'Extension "{extension_name}" not found')

    def test_version_upload_success(self):
        self.assertEqual(Version.objects.filter(extension=self.extension).count(), 1)
        with open(self.file_path, 'rb') as version_file:
            response = self.client.post(
                self._get_upload_url(self.extension.extension_id),
                {
                    'version_file': version_file,
                    'release_notes': 'These are the release notes',
                },
                format='multipart',
                HTTP_AUTHORIZATION=f'Bearer {self.token_key}',
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Version.objects.filter(extension=self.extension).count(), 2)

    def test_date_last_access(self):
        self.assertIsNone(self.token.date_last_access)
        with open(self.file_path, 'rb') as version_file:
            response = self.client.post(
                self._get_upload_url(self.extension.extension_id),
                {
                    'version_file': version_file,
                    'release_notes': 'These are the release notes',
                },
                format='multipart',
                HTTP_AUTHORIZATION=f'Bearer {self.token_key}',
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.token.refresh_from_db()
        self.assertIsNotNone(self.token.date_last_access)
