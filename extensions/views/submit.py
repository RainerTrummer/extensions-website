import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.views.generic.edit import CreateView

from extensions.models import Extension
from files.forms import FileForm
from files.models import File

logger = logging.getLogger(__name__)


class UploadFileView(LoginRequiredMixin, CreateView):
    model = File
    template_name = 'extensions/submit.html'
    form_class = FileForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        drafts = Extension.objects.authored_by(self.request.user).filter(
            status=Extension.STATUSES.DRAFT
        )
        context['drafts'] = drafts
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_success_url(self):
        return self.extension.get_draft_url()

    @transaction.atomic
    def form_valid(self, form):
        """Create an extension and a version already, associated with the user."""
        file = form.save()
        self.extension = Extension.create_from_file(file)
        self.extension.create_version_from_file(file)
        return super().form_valid(form)
