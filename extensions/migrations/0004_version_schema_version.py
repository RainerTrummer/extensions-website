# Generated by Django 4.0.6 on 2024-02-02 08:49

from django.db import migrations
import extensions.fields


class Migration(migrations.Migration):

    dependencies = [
        ('extensions', '0003_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='version',
            name='schema_version',
            field=extensions.fields.VersionStringField(coerce=False, default='1.0.0', help_text='Specification version the manifest file is following.', max_length=64, partial=False),
        ),
    ]
